package numbernolodin.com.charts.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import numbernolodin.com.charts.Main2Activity;
import numbernolodin.com.charts.R;

/**
 * Created by Coder on 14.03.2019.
 */

public class CustomView extends View {
    public boolean first,second,third,fourth,fifth,charty0,charty1,charty2,charty3,charty4,charty5,charty6;
    public float endRect,startRect;
    public String [] Y0 ,Y1,Y2,Y3,Y4,Y5,Y6,X,MmY0 ,MmY1,MmY2,MmY3,MmY4,MmY5;
    int[] values,values0,values1,suma;
    long[] dates;
    float differenceStart,differenceStop;
    Path path;
    public int selectedPosition,arrayItemN;
    int a;


    Paint r,rm, p,c1, c2,c3,c4,c5,c6,c7,gray;
    Rect rectLeft,rectRight;

    public static long date = 1,
            datemax = 1,
            datemin = 1,
            followers = 1,
            followers0 = 1,
            followers1 = 1,
            followersmax = 1,
            followersmin = 1,
            dateMinimap = 1,
            datemaxMinimap = 1,
            dateminMinimap = 1,
            followersMinimap = 1,
            followersmaxMinimap = 1,
            followersminMinimap = 1,
            followersmax0,
            followersmax1,
            followersmin0,
            followersmin1,
            followersmax0Minimap,
            followersmax1Minimap,
            followersmin0Minimap,
            followersmin1Minimap,
            followersMinimap0,
            followersMinimap1;
    public CustomView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }


    public CustomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CustomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public CustomView(Context context) {
        super(context);
    }


    public String getChart(){
        if(first){
            return "first";
        }
        else if(second)
        {
            return "second";
        }
        else if(third)
        {
            return "third";
        }
        else if(fourth)
        {
            return "fourth";
        }
        else if(fifth)
        {
            return "fifth";
        }
        return null;
    }
        @Override
        protected void onDraw(Canvas canvas) {
            first = true;
            if(endRect== 0 && startRect == 0){
            endRect = x(90);
            startRect = x(990);
            }
            p = new Paint(Paint.ANTI_ALIAS_FLAG);
            r = new Paint(Paint.ANTI_ALIAS_FLAG);
            rm = new Paint(Paint.ANTI_ALIAS_FLAG);
            c1 = new Paint(Paint.ANTI_ALIAS_FLAG);
            c2 = new Paint(Paint.ANTI_ALIAS_FLAG);
            c3 = new Paint(Paint.ANTI_ALIAS_FLAG);
            c4 = new Paint(Paint.ANTI_ALIAS_FLAG);
            c5 = new Paint(Paint.ANTI_ALIAS_FLAG);
            c6 = new Paint(Paint.ANTI_ALIAS_FLAG);
            c7 = new Paint(Paint.ANTI_ALIAS_FLAG);
            gray = new Paint(Paint.ANTI_ALIAS_FLAG);
            rectLeft = new Rect();
            rectRight = new Rect();

            TextPaint title = new TextPaint();
            title.setTextSize(x(50));
            title.setTextAlign(Paint.Align.LEFT);
            title.setTypeface(Typeface.create("Arial", Typeface.BOLD));

            TextPaint period = new TextPaint();
            period.setTextSize(x(40));
            period.setTextAlign(Paint.Align.RIGHT);
            period.setTypeface(Typeface.create("Arial", Typeface.BOLD));

            if(Main2Activity.night_mode){
                period.setColor(Color.WHITE);
                title.setColor(Color.WHITE);
            }
            else{
                period.setColor(Color.BLACK);
                title.setColor(Color.BLACK);
            }

            //endregion
            values = new int[6];
            values0 = new int[6];
            values1 = new int[6];
            dates = new long[6];



            r.setColor(getResources().getColor(R.color.gray));
            r.setStrokeWidth(3);

            rm.setColor(getResources().getColor(R.color.MiddleRect));
            rm.setStrokeWidth(8);
            rm.setStyle(Paint.Style.STROKE);

            p.setColor(Color.GRAY);
            p.setStrokeWidth(3);
            p.setStyle(Paint.Style.STROKE);


            TextPaint textPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
            textPaint.setTextSize(25);
            textPaint.setTextAlign(Paint.Align.LEFT);
            textPaint.setColor(Color.GRAY);
            textPaint.setTypeface(Typeface.create("Arial", Typeface.NORMAL));

            c1.setColor(Color.RED);
            c1.setStrokeWidth(4);

            c2.setColor(Color.RED);
            c2.setStrokeWidth(4);

            c3.setColor(Color.RED);
            c3.setStrokeWidth(4);

            c4.setColor(Color.RED);
            c4.setStrokeWidth(4);

            c5.setColor(Color.RED);
            c5.setStrokeWidth(4);

            c6.setColor(Color.RED);
            c6.setStrokeWidth(4);

            c7.setColor(Color.RED);
            c7.setStrokeWidth(4);

            gray.setColor(Color.GRAY);
            gray.setStrokeWidth(4);

            int endRectDraw = x((int)endRect);
            int startRectDraw = x((int)startRect);
            //canvas.drawLine(x(100), y(1166), x(1000),y(1166), p);
            rectLeft.set(x(90), y(1230), (int)Scale(Float.valueOf(endRectDraw)), y(1350));
            rectRight.set((int)Scale(Float.valueOf(startRectDraw)), y(1230), x(990), y(1350));




            if (first) {
                datemaxMinimap = Max(Main2Activity.FirstChartX);
                dateminMinimap = Min(Main2Activity.FirstChartX);
                followersmaxMinimap = Max(Main2Activity.FirstChartY0, Main2Activity.FirstChartY1);
                followersminMinimap = Min(Main2Activity.FirstChartY0, Main2Activity.FirstChartY1);
                followersMinimap = followersmaxMinimap - followersminMinimap;
                dateMinimap = datemaxMinimap - dateminMinimap;


                c1.setColor(getResources().getColor(R.color.colorY0));
                c2.setColor(getResources().getColor(R.color.colorY1));

                differenceStart = Math.abs(x(90) - endRect);
                differenceStop = (x(990) - startRect);
                float startCoef = differenceStart / (x(900));
                float endCoef = differenceStop / (x(1000));
                Y0 = new String[Main2Activity.FirstChartY0.length - (int) (Main2Activity.FirstChartY0.length * endCoef) - (int) (Main2Activity.FirstChartY0.length * startCoef)];
                Y1 = new String[Y0.length];
                X = new String[Y0.length];
                a = 0;
                int starter = (int) (Main2Activity.FirstChartY0.length * startCoef);
                for (int i = 0 + starter; i < Main2Activity.FirstChartY0.length - ((int) ((Main2Activity.FirstChartY0.length * endCoef))); i++) {
                    Y0[a] = Main2Activity.FirstChartY0[i];
                    Y1[a] = Main2Activity.FirstChartY1[i];
                    X[a] = Main2Activity.FirstChartX[i];
                    a = a + 1;
                }
                if(!charty0){
                    Y0 = arrayToZero(Y0);
                }if(!charty1){
                    Y1 = arrayToZero(Y1);
                }
                datemax = Max(X);
                datemin = Min(X);
                canvas.drawText("Followers",x(100),y(100),title);
                SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM YYYY", Locale.ENGLISH);
                Date startDate = new Date(datemin);
                Date stopDate = new Date(datemax);
                canvas.drawText(formatter.format(startDate)+" - "+formatter.format(stopDate),x(990),y(100),period);
                followersmax = Max(Y0, Y1);
                followersmin = Min(Y0, Y1);

                followers = followersmax - followersmin;
                date = datemax - datemin;

                if (charty0) {
                    for (int i = 0; i < Y0.length - 1/*FirstChartY0.length-1*/; i++) {
                        int startX, startY, stopX, stopY;
                        startY = (int) (y(1130) - (Long.valueOf(/*FirstChart*/Y0[i])-followersmin) * y(1000) / followers);
                        startX = (int) ((Long.valueOf(/*FirstChart*/X[i]) - datemin) * x(900) / date) + x(90);
                        stopY = (int) (y(1130) - (Long.valueOf(/*FirstChart*/Y0[i + 1])-followersmin) * y(1000) / followers);
                        stopX = (int) ((Long.valueOf(/*FirstChart*/X[i + 1]) - datemin) * x(900) / date) + x(90);
                        canvas.drawLine(startX, startY, stopX, stopY, c1);
                    }
                }
                if (charty1) {
                    for (int i = 0; i < Y1.length - 1; i++) {
                        int startX, startY, stopX, stopY;
                        startY = (int) (y(1130) - (Long.valueOf(/*FirstChart*/Y1[i])-followersmin) * y(1000) / followers);
                        startX = (int) ((Long.valueOf(/*FirstChart*/X[i]) - datemin) * x(900) / date) + x(90);
                        stopY = (int) (y(1130) - (Long.valueOf(/*FirstChart*/Y1[i + 1])-followersmin) * y(1000) / followers);
                        stopX = (int) ((Long.valueOf(/*FirstChart*/X[i + 1]) - datemin) * x(900) / date) + x(90);
                        canvas.drawLine(startX, startY, stopX, stopY, c2);
                    }
                }
                //мини вид v
                for (int i = 0; i < Main2Activity.FirstChartY0.length - 1; i++) {
                    int startX, startY, stopX, stopY;
                    startY = (int) (y(1350) - (Long.valueOf(Main2Activity.FirstChartY0[i])-followersminMinimap) * y(120) / followersMinimap);
                    startX = (int) ((Long.valueOf(Main2Activity.FirstChartX[i]) - dateminMinimap) * x(900) / dateMinimap) + x(90);
                    stopY = (int) (y(1350) - (Long.valueOf(Main2Activity.FirstChartY0[i + 1])-followersminMinimap) * y(120) / followersMinimap);
                    stopX = (int) ((Long.valueOf(Main2Activity.FirstChartX[i + 1]) - dateminMinimap) * x(900) / dateMinimap) + x(90);
                    canvas.drawLine(startX, startY, stopX, stopY, c1);
                }
                for (int i = 0; i < Main2Activity.FirstChartY0.length - 1; i++) {
                    int startX, startY, stopX, stopY;
                    startY = (int) (y(1350) - (Long.valueOf(Main2Activity.FirstChartY1[i])-followersminMinimap) * y(120) / followersMinimap);
                    startX = (int) ((Long.valueOf(Main2Activity.FirstChartX[i]) - dateminMinimap) * x(900) / dateMinimap) + x(90);
                    stopY = (int) (y(1350) - (Long.valueOf(Main2Activity.FirstChartY1[i + 1])-followersminMinimap) * y(120) / followersMinimap);
                    stopX = (int) ((Long.valueOf(Main2Activity.FirstChartX[i + 1]) - dateminMinimap) * x(900) / dateMinimap) + x(90);
                    canvas.drawLine(startX, startY, stopX, stopY, c2);
                }
                //мини вид^
                for (int i = 0; i <= 5; i++) {
                    values[i] = (int)followersmin + (int) (followers / 6) * i;
                    dates[i] = datemin + (date / 6) * i;
                    canvas.drawText(String.valueOf(values[i]), x(90), y(1130) - (y(166) * i) - y(20), textPaint);
                    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd", Locale.ENGLISH);
                    Date d = new Date(dates[i]);
                    canvas.drawText(sdf.format(d), x(90) + x(900) / 6 * i, y(1170), textPaint);
                }
            }//FirstChart
            else if(second==true){

                datemaxMinimap = Max(Main2Activity.SecondChartX);
                dateminMinimap = Min(Main2Activity.SecondChartX);
                followersmax0Minimap = Max(Main2Activity.SecondChartY0);
                followersmin0Minimap = Min(Main2Activity.SecondChartY0);
                followersmax1Minimap = Max(Main2Activity.SecondChartY1);
                followersmin1Minimap = Min(Main2Activity.SecondChartY1);
                followersMinimap0 = followersmax0Minimap - followersmin0Minimap;
                followersMinimap1 = followersmax1Minimap - followersmin1Minimap;
                dateMinimap = datemaxMinimap - dateminMinimap;


                //Масштаб Y= 1000 / followers;
                //Масштаб X = 900 / date;
                c1.setColor(getResources().getColor(R.color.color2Y0));
                c2.setColor(getResources().getColor(R.color.color2Y1));

                differenceStart=Math.abs(x(90)-endRect);
                differenceStop=(x(990)-startRect);
                float startCoef = differenceStart/(x(900));
                float endCoef = differenceStop/(x(1000));
                Y0 = new String[Main2Activity.SecondChartY0.length-(int)(Main2Activity.SecondChartY0.length*endCoef)-(int)(Main2Activity.SecondChartY0.length*startCoef)];
                Y1 = new String[Y0.length];
                X = new String[Y0.length];
                a=0;
                for(int i = 0+(int)(Main2Activity.SecondChartY0.length*startCoef);i<Main2Activity.SecondChartY0.length-(int)((Main2Activity.SecondChartY0.length*endCoef));i++){
                    Y0[a]=Main2Activity.SecondChartY0[i];
                    Y1[a]=Main2Activity.SecondChartY1[i];
                    X[a] =Main2Activity.SecondChartX[i];
                    a=a+1;
                }
                if(!charty0){
                    Y0 = arrayToZero(Y0);
                }if(!charty1){
                    Y1 = arrayToZero(Y1);
                }
                datemax = Max(X);
                datemin = Min(X);
                canvas.drawText("Interactions",x(100),y(100),title);
                SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM YYYY", Locale.ENGLISH);
                Date startDate = new Date(datemin);
                Date stopDate = new Date(datemax);
                canvas.drawText(formatter.format(startDate)+" - "+formatter.format(stopDate),x(990),y(100),period);
                followersmax0 = Max(Y0);
                followersmin0 = Min(Y0);
                followersmax1 = Max(Y1);
                followersmin1 = Min(Y1);

                followers0 = followersmax0 - followersmin0;
                followers1 = followersmax1 - followersmin1;
                date = datemax - datemin;
                if(charty0) {
                    for (int i = 0; i < Y0.length - 1/*Main2Activity.SecondChartY0.length-1*/; i++) {
                        int startX, startY, stopX, stopY;
                        startY = (int) (y(1130) - (Long.valueOf(/*Main2Activity.SecondChart*/Y0[i])-followersmin0) * y(1000) / followers0);
                        startX = (int) ((Long.valueOf(/*Main2Activity.SecondChart*/X[i]) - datemin) * x(900) / date) + x(90);
                        stopY = (int) (y(1130) - (Long.valueOf(/*Main2Activity.SecondChart*/Y0[i + 1])-followersmin0) * y(1000) / followers0);
                        stopX = (int) ((Long.valueOf(/*Main2Activity.SecondChart*/X[i + 1]) - datemin) * x(900) / date) + x(90);
                        canvas.drawLine(startX, startY, stopX, stopY, c1);
                    }
                }
                if(charty1) {
                    for (int i = 0; i < Y0.length - 1; i++) {
                        int startX, startY, stopX, stopY;
                        startY = (int) (y(1130) - (Long.valueOf(/*Main2Activity.SecondChart*/Y1[i])-followersmin1) * y(1000) / followers1);
                        startX = (int) ((Long.valueOf(/*Main2Activity.SecondChart*/X[i]) - datemin) * x(900) / date) + x(90);
                        stopY = (int) (y(1130) - (Long.valueOf(/*Main2Activity.SecondChart*/Y1[i + 1])-followersmin1) * y(1000) / followers1);
                        stopX = (int) ((Long.valueOf(/*Main2Activity.SecondChart*/X[i + 1]) - datemin) * x(900) / date) + x(90);
                        canvas.drawLine(startX, startY, stopX, stopY, c2);
                    }
                }
                //мини вид v
                for(int i = 0; i<Main2Activity.SecondChartY0.length-1;i++){
                    int startX,startY,stopX,stopY;
                    startY = (int)(y(1350)-(Long.valueOf(Main2Activity.SecondChartY0[i])-followersmin0Minimap)*y(120)/followersMinimap0);
                    startX = (int)((Long.valueOf(Main2Activity.SecondChartX[i])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    stopY = (int)(y(1350)-(Long.valueOf(Main2Activity.SecondChartY0[i+1])-followersmin0Minimap)*y(120)/followersMinimap0);
                    stopX = (int)((Long.valueOf(Main2Activity.SecondChartX[i+1])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    canvas.drawLine(startX,startY,stopX,stopY,c1);
                }
                for(int i = 0; i<Main2Activity.SecondChartY0.length-1;i++){
                    int startX,startY,stopX,stopY;
                    startY = (int)(y(1350)-(Long.valueOf(Main2Activity.SecondChartY1[i])-followersmin1Minimap)*y(120)/followersMinimap1);
                    startX = (int)((Long.valueOf(Main2Activity.SecondChartX[i])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    stopY = (int)(y(1350)-(Long.valueOf(Main2Activity.SecondChartY1[i+1])-followersmin1Minimap)*y(120)/followersMinimap1);
                    stopX = (int)((Long.valueOf(Main2Activity.SecondChartX[i+1])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    canvas.drawLine(startX,startY,stopX,stopY,c2);
                }
                //мини вид^
                TextPaint textPaintChart2Y0 = new TextPaint(Paint.ANTI_ALIAS_FLAG);
                textPaintChart2Y0.setTextSize(25);
                textPaintChart2Y0.setTextAlign(Paint.Align.LEFT);
                textPaintChart2Y0.setColor(getResources().getColor(R.color.color2Y0));
                textPaintChart2Y0.setTypeface(Typeface.create("Arial", Typeface.NORMAL));

                TextPaint textPaintChart2Y1 = new TextPaint(Paint.ANTI_ALIAS_FLAG);
                textPaintChart2Y1.setTextSize(25);
                textPaintChart2Y1.setTextAlign(Paint.Align.LEFT);
                textPaintChart2Y1.setColor(getResources().getColor(R.color.color2Y1));
                textPaintChart2Y1.setTypeface(Typeface.create("Arial", Typeface.NORMAL));
                for(int i = 0;i<=5;i++){
                    values0[i] = (int)followersmin0 + (int) (followers0/ 6) * i;
                    values1[i]=(int)followersmin1 + (int) (followers1/6)*i;
                    dates[i] = datemin +(date/6)*i;
                    if(charty0)canvas.drawText(String.valueOf(values0[i]),x(90),y(1130)-(y(166)*i)-y(20),textPaintChart2Y0);
                    if(charty1)canvas.drawText(String.valueOf(values1[i]),x(900),y(1130)-(y(166)*i)-y(20),textPaintChart2Y1);
                    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd", Locale.ENGLISH);
                    Date d = new Date(dates[i]);
                    canvas.drawText(sdf.format(d),x(90)+x(900)/6*i,y(1170),textPaint);
                }


            }//SecondChart
            else if(third==true){

                datemaxMinimap = Max(Main2Activity.ThirdChartX);
                dateminMinimap = Min(Main2Activity.ThirdChartX);
                followersmaxMinimap = Max(Main2Activity.ThirdChartY0, Main2Activity.ThirdChartY1,Main2Activity.ThirdChartY2, Main2Activity.ThirdChartY3,Main2Activity.ThirdChartY4, Main2Activity.ThirdChartY5,Main2Activity.ThirdChartY6);
                followersminMinimap = Min(Main2Activity.ThirdChartY0, Main2Activity.ThirdChartY1,Main2Activity.ThirdChartY2, Main2Activity.ThirdChartY3,Main2Activity.ThirdChartY4, Main2Activity.ThirdChartY5,Main2Activity.ThirdChartY6);
                followersMinimap = followersmaxMinimap - followersminMinimap;
                dateMinimap = datemaxMinimap - dateminMinimap;


                //Масштаб Y= 1000 / followers;
                //Масштаб X = 900 / date;
                Paint c1Copy = new Paint();
                Paint c2Copy = new Paint();
                Paint c3Copy = new Paint();
                Paint c4Copy = new Paint();
                Paint c5Copy = new Paint();
                Paint c6Copy = new Paint();
                Paint c7Copy = new Paint();
                c1Copy.setColor(getResources().getColor(R.color.color3Y0));
                c2Copy.setColor(getResources().getColor(R.color.color3Y1));
                c3Copy.setColor(getResources().getColor(R.color.color3Y2));
                c4Copy.setColor(getResources().getColor(R.color.color3Y3));
                c5Copy.setColor(getResources().getColor(R.color.color3Y4));
                c6Copy.setColor(getResources().getColor(R.color.color3Y5));
                c7Copy.setColor(getResources().getColor(R.color.color3Y6));
                if(selectedPosition==0){
                    c1.setColor(getResources().getColor(R.color.color3Y0));
                    c2.setColor(getResources().getColor(R.color.color3Y1));
                    c3.setColor(getResources().getColor(R.color.color3Y2));
                    c4.setColor(getResources().getColor(R.color.color3Y3));
                    c5.setColor(getResources().getColor(R.color.color3Y4));
                    c6.setColor(getResources().getColor(R.color.color3Y5));
                    c7.setColor(getResources().getColor(R.color.color3Y6));
                }
                else {
                    c1.setColor(getResources().getColor(R.color.color3Y0T));
                    c2.setColor(getResources().getColor(R.color.color3Y1T));
                    c3.setColor(getResources().getColor(R.color.color3Y2T));
                    c4.setColor(getResources().getColor(R.color.color3Y3T));
                    c5.setColor(getResources().getColor(R.color.color3Y4T));
                    c6.setColor(getResources().getColor(R.color.color3Y5T));
                    c7.setColor(getResources().getColor(R.color.color3Y6T));
                    arrayItemN = (int)Main2Activity.clamp((Y0.length*(selectedPosition-x(90))/x(900)),0,Y0.length-1);
                }



                differenceStart=Math.abs(x(90)-endRect);
                differenceStop=(x(990)-startRect);
                float startCoef = differenceStart/(x(900));
                float endCoef = differenceStop/(x(1000));
                Y0 = new String[Main2Activity.ThirdChartY0.length-(int)(Main2Activity.ThirdChartY0.length*endCoef)-(int)(Main2Activity.ThirdChartY0.length*startCoef)];
                Y1 = new String[Y0.length];
                Y2 = new String[Y0.length];
                Y3 = new String[Y0.length];
                Y4 = new String[Y0.length];
                Y5 = new String[Y0.length];
                Y6 = new String[Y0.length];
                X = new String[Y0.length];
                a=0;
                for(int i = 0+(int)(Main2Activity.ThirdChartY0.length*startCoef);i<Main2Activity.ThirdChartY0.length-(int)((Main2Activity.ThirdChartY0.length*endCoef));i++){
                    Y0[a]=Main2Activity.ThirdChartY0[i];
                    Y1[a]=Main2Activity.ThirdChartY1[i];
                    Y2[a]=Main2Activity.ThirdChartY2[i];
                    Y3[a]=Main2Activity.ThirdChartY3[i];
                    Y4[a]=Main2Activity.ThirdChartY4[i];
                    Y5[a]=Main2Activity.ThirdChartY5[i];
                    Y6[a]=Main2Activity.ThirdChartY6[i];
                    X[a] =Main2Activity.ThirdChartX[i];
                    a=a+1;
                }
                if(!charty0){
                    Y0 = arrayToZero(Y0);
                }if(!charty1){
                    Y1 = arrayToZero(Y1);
                }if(!charty2){
                    Y2 = arrayToZero(Y2);
                }if(!charty3){
                    Y3 = arrayToZero(Y3);
                }if(!charty4){
                    Y4 = arrayToZero(Y4);
                }if(!charty5){
                    Y5 = arrayToZero(Y5);
                }if(!charty6){
                    Y6 = arrayToZero(Y6);
                }
                datemax = Max(X);
                datemin = Min(X);
                canvas.drawText("Fruits",x(100),y(100),title);
                SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM YYYY", Locale.ENGLISH);
                Date startDate = new Date(datemin);
                Date stopDate = new Date(datemax);
                canvas.drawText(formatter.format(startDate)+" - "+formatter.format(stopDate),x(990),y(100),period);
                followersmax = Max(Y0, Y1, Y2, Y3, Y4, Y5, Y6);
                followersmin = Min(Y0, Y1, Y2, Y3, Y4, Y5, Y6);
                followers = followersmax - followersmin;
                date = datemax - datemin;

                    for (int i = 0; i < Y0.length - 1/*Main2Activity.ThirdChartY0.length-1*/; i++) {
                        int startX, startY, stopX, stopY;
                        if(charty0) {
                            if(i!=arrayItemN) {
                                startY = (int) (y(1130) - (Long.valueOf(/*Main2Activity.ThirdChart*/Y0[i]) * y(1000) / followersmax));
                                startX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i]) - datemin) * x(900) / date) + x(90);
                                stopY = y(1130);
                                stopX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i + 1]) - datemin) * x(900) / date) + x(90);
                                canvas.drawRect(startX, startY, stopX, stopY, c1);
                            }
                            else{
                                startY = (int) (y(1130) - (Long.valueOf(/*Main2Activity.ThirdChart*/Y0[i]) * y(1000) / followersmax));
                                startX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i]) - datemin) * x(900) / date) + x(90);
                                stopY = y(1130);
                                stopX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i + 1]) - datemin) * x(900) / date) + x(90);
                                canvas.drawRect(startX, startY, stopX, stopY, c1Copy);
                            }
                        }
                        if(charty2) {
                            if(i!=arrayItemN) {
                                startY = (int) (y(1130) - Long.valueOf(/*Main2Activity.ThirdChart*/Y2[i]) * y(1000) / followersmax);
                                startX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i]) - datemin) * x(900) / date) + x(90);
                                stopY = y(1130);
                                stopX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i + 1]) - datemin) * x(900) / date) + x(90);
                                canvas.drawRect(startX, startY, stopX, stopY, c3);
                            }
                            else {
                                startY = (int) (y(1130) - Long.valueOf(/*Main2Activity.ThirdChart*/Y2[i]) * y(1000) / followersmax);
                                startX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i]) - datemin) * x(900) / date) + x(90);
                                stopY = y(1130);
                                stopX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i + 1]) - datemin) * x(900) / date) + x(90);
                                canvas.drawRect(startX, startY, stopX, stopY, c3Copy);
                            }
                    }
                        if(charty1) {
                            if(i!=arrayItemN) {
                                startY = (int) (y(1130) - Long.valueOf(/*Main2Activity.ThirdChart*/Y1[i]) * y(1000) / followersmax);
                                startX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i]) - datemin) * x(900) / date) + x(90);
                                stopY = y(1130);
                                stopX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i + 1]) - datemin) * x(900) / date) + x(90);
                                canvas.drawRect(startX, startY, stopX, stopY, c2);
                            }else {
                                startY = (int) (y(1130) - Long.valueOf(/*Main2Activity.ThirdChart*/Y1[i]) * y(1000) / followersmax);
                                startX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i]) - datemin) * x(900) / date) + x(90);
                                stopY = y(1130);
                                stopX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i + 1]) - datemin) * x(900) / date) + x(90);
                                canvas.drawRect(startX, startY, stopX, stopY, c2Copy);
                            }
                        }
                        if(charty5) {
                            if(i!=arrayItemN) {
                                startY = (int) (y(1130) - Long.valueOf(/*Main2Activity.ThirdChart*/Y5[i]) * y(1000) / followersmax);
                                startX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i]) - datemin) * x(900) / date) + x(90);
                                stopY = y(1130);
                                stopX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i + 1]) - datemin) * x(900) / date) + x(90);
                                canvas.drawRect(startX, startY, stopX, stopY, c6);
                            }else {
                                startY = (int) (y(1130) - Long.valueOf(/*Main2Activity.ThirdChart*/Y5[i]) * y(1000) / followersmax);
                                startX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i]) - datemin) * x(900) / date) + x(90);
                                stopY = y(1130);
                                stopX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i + 1]) - datemin) * x(900) / date) + x(90);
                                canvas.drawRect(startX, startY, stopX, stopY, c6Copy);
                            }
                        }
                        if(charty4) {
                            if(i!=arrayItemN) {
                                startY = (int) (y(1130) - Long.valueOf(/*Main2Activity.ThirdChart*/Y4[i]) * y(1000) / followersmax);
                                startX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i]) - datemin) * x(900) / date) + x(90);
                                stopY = y(1130);
                                stopX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i + 1]) - datemin) * x(900) / date) + x(90);
                                canvas.drawRect(startX, startY, stopX, stopY, c5);
                            }else {
                                startY = (int) (y(1130) - Long.valueOf(/*Main2Activity.ThirdChart*/Y4[i]) * y(1000) / followersmax);
                                startX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i]) - datemin) * x(900) / date) + x(90);
                                stopY = y(1130);
                                stopX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i + 1]) - datemin) * x(900) / date) + x(90);
                                canvas.drawRect(startX, startY, stopX, stopY, c5Copy);
                            }
                        }
                        if(charty3) {
                            if(i!=arrayItemN) {
                                startY = (int) (y(1130) - Long.valueOf(/*Main2Activity.ThirdChart*/Y3[i]) * y(1000) / followersmax);
                                startX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i]) - datemin) * x(900) / date) + x(90);
                                stopY = y(1130);
                                stopX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i + 1]) - datemin) * x(900) / date) + x(90);
                                canvas.drawRect(startX, startY, stopX, stopY, c4);
                            }else {
                                startY = (int) (y(1130) - Long.valueOf(/*Main2Activity.ThirdChart*/Y3[i]) * y(1000) / followersmax);
                                startX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i]) - datemin) * x(900) / date) + x(90);
                                stopY = y(1130);
                                stopX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i + 1]) - datemin) * x(900) / date) + x(90);
                                canvas.drawRect(startX, startY, stopX, stopY, c4Copy);
                            }
                        }
                        if(charty6) {
                            if(i!=arrayItemN) {
                                startY = (int) (y(1130) - Long.valueOf(/*Main2Activity.ThirdChart*/Y6[i]) * y(1000) / followersmax);
                                startX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i]) - datemin) * x(900) / date) + x(90);
                                stopY = y(1130);
                                stopX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i + 1]) - datemin) * x(900) / date) + x(90);
                                canvas.drawRect(startX, startY, stopX, stopY, c7);
                            }else {
                                startY = (int) (y(1130) - Long.valueOf(/*Main2Activity.ThirdChart*/Y6[i]) * y(1000) / followersmax);
                                startX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i]) - datemin) * x(900) / date) + x(90);
                                stopY = y(1130);
                                stopX = (int) ((Long.valueOf(/*Main2Activity.ThirdChart*/X[i + 1]) - datemin) * x(900) / date) + x(90);
                                canvas.drawRect(startX, startY, stopX, stopY, c7Copy);
                            }
                        }
                }
                //мини вид v
                for(int i = 0; i<Main2Activity.ThirdChartY0.length-1;i++){
                    int startX,startY,stopX,stopY;
                    startY = (int)(y(1350)-(Long.valueOf(Main2Activity.ThirdChartY0[i])*y(120)/followersmaxMinimap));
                    startX = (int)((Long.valueOf(Main2Activity.ThirdChartX[i])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    stopY = y(1350);
                    stopX = (int)((Long.valueOf(Main2Activity.ThirdChartX[i+1])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    canvas.drawRect(startX,startY,stopX,stopY,c1);

                    startY = (int)(y(1350)-Long.valueOf(Main2Activity.ThirdChartY1[i])*y(120)/followersmaxMinimap);
                    startX = (int)((Long.valueOf(Main2Activity.ThirdChartX[i])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    stopY = y(1350);
                    stopX = (int)((Long.valueOf(Main2Activity.ThirdChartX[i+1])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    canvas.drawRect(startX,startY,stopX,stopY,c2);

                    startY = (int)(y(1350)-Long.valueOf(Main2Activity.ThirdChartY2[i])*y(120)/followersmaxMinimap);
                    startX = (int)((Long.valueOf(Main2Activity.ThirdChartX[i])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    stopY = y(1350);
                    stopX = (int)((Long.valueOf(Main2Activity.ThirdChartX[i+1])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    canvas.drawRect(startX,startY,stopX,stopY,c3);

                    startY = (int)(y(1350)-Long.valueOf(Main2Activity.ThirdChartY3[i])*y(120)/followersmaxMinimap);
                    startX = (int)((Long.valueOf(Main2Activity.ThirdChartX[i])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    stopY = y(1350);
                    stopX = (int)((Long.valueOf(Main2Activity.ThirdChartX[i+1])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    canvas.drawRect(startX,startY,stopX,stopY,c4);

                    startY = (int)(y(1350)-Long.valueOf(Main2Activity.ThirdChartY4[i])*y(120)/followersmaxMinimap);
                    startX = (int)((Long.valueOf(Main2Activity.ThirdChartX[i])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    stopY = y(1350);
                    stopX = (int)((Long.valueOf(Main2Activity.ThirdChartX[i+1])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    canvas.drawRect(startX,startY,stopX,stopY,c5);

                    startY = (int)(y(1350)-Long.valueOf(Main2Activity.ThirdChartY5[i])*y(120)/followersmaxMinimap);
                    startX = (int)((Long.valueOf(Main2Activity.ThirdChartX[i])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    stopY = y(1350);
                    stopX = (int)((Long.valueOf(Main2Activity.ThirdChartX[i+1])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    canvas.drawRect(startX,startY,stopX,stopY,c6);

                    startY = (int)(y(1350)-Long.valueOf(Main2Activity.ThirdChartY6[i])*y(120)/followersmaxMinimap);
                    startX = (int)((Long.valueOf(Main2Activity.ThirdChartX[i])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    stopY = y(1350);
                    stopX = (int)((Long.valueOf(Main2Activity.ThirdChartX[i+1])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    canvas.drawRect(startX,startY,stopX,stopY,c7);
                }

                //мини вид^
                for(int i = 0;i<=5;i++){
                    values[i]=0 + (int) (followersmax/6)*i;
                    dates[i] = datemin +(date/6)*i;
                    canvas.drawText(String.valueOf(values[i]),x(90),y(1130)-(y(166)*i)-y(20),textPaint);
                    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd", Locale.ENGLISH);
                    Date d = new Date(dates[i]);
                    canvas.drawText(sdf.format(d),x(90)+x(900)/6*i,y(1170),textPaint);
                }

            }//ThirdChart
            else if(fourth==true){
                datemaxMinimap = Max(Main2Activity.FourthChartX);
                dateminMinimap = Min(Main2Activity.FourthChartX);
                followersmaxMinimap = Max(Main2Activity.FourthChartY0);
                followersminMinimap = Min(Main2Activity.FourthChartY0);
                followersMinimap = followersmaxMinimap - followersminMinimap;
                dateMinimap = datemaxMinimap - dateminMinimap;


                //Масштаб Y= 1000 / followers;
                //Масштаб X = 900 / date;
                Paint c1Copy = new Paint();
                c1Copy.setColor(getResources().getColor(R.color.color4Y0));
                if(selectedPosition==0){c1.setColor(getResources().getColor(R.color.color4Y0));}
                else {
                    c1.setColor(getResources().getColor(R.color.color4Y0T));
                    arrayItemN = (int)Main2Activity.clamp((Y0.length*(selectedPosition-x(90))/x(900)),0,Y0.length-1);
                }

                differenceStart=Math.abs(x(90)-endRect);
                differenceStop=(x(990)-startRect);
                float startCoef = differenceStart/(x(900));
                float endCoef = differenceStop/(x(1000));
                Y0 = new String[Main2Activity.FourthChartY0.length-(int)(Main2Activity.FourthChartY0.length*endCoef)-(int)(Main2Activity.FourthChartY0.length*startCoef)];
                X = new String[Y0.length];
                a=0;
                for(int i = 0+(int)(Main2Activity.FourthChartY0.length*startCoef);i<Main2Activity.FourthChartY0.length-(int)((Main2Activity.FourthChartY0.length*endCoef));i++){
                    Y0[a]=Main2Activity.FourthChartY0[i];
                    X[a] =Main2Activity.FourthChartX[i];
                    a=a+1;
                }
                datemax = Max(X);
                datemin = Min(X);
                canvas.drawText("Views",x(100),y(100),title);
                SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM YYYY", Locale.ENGLISH);
                Date startDate = new Date(datemin);
                Date stopDate = new Date(datemax);
                canvas.drawText(formatter.format(startDate)+" - "+formatter.format(stopDate),x(990),y(100),period);
                followersmax = Max(Y0);
                followersmin = Min(Y0);

                followers = followersmax - followersmin;
                date = datemax - datemin;
                if(charty0) {
                    for (int i = 0; i < Y0.length - 1/*Main2Activity.FourthChartY0.length-1*/; i++) {
                        if(i!=arrayItemN) {
                            int startX, startY, stopX, stopY;
                            startY = (int) (y(1130) - (Long.valueOf(/*Main2Activity.FourthChart*/Y0[i]) * y(1000) / followersmax));
                            startX = (int) ((Long.valueOf(/*Main2Activity.FourthChart*/X[i]) - datemin) * x(900) / date) + x(90);
                            stopY = (y(1130));
                            stopX = (int) ((Long.valueOf(/*Main2Activity.FourthChart*/X[i + 1]) - datemin) * x(900) / date) + x(90);
                            canvas.drawRect(startX, startY, stopX, stopY, c1);
                        }
                        else{
                            int startX, startY, stopX, stopY;
                            startY = (int) (y(1130) - (Long.valueOf(/*Main2Activity.FourthChart*/Y0[i]) * y(1000) / followersmax));
                            startX = (int) ((Long.valueOf(/*Main2Activity.FourthChart*/X[i]) - datemin) * x(900) / date) + x(90);
                            stopY = (y(1130));
                            stopX = (int) ((Long.valueOf(/*Main2Activity.FourthChart*/X[i + 1]) - datemin) * x(900) / date) + x(90);
                            canvas.drawRect(startX, startY, stopX, stopY, c1Copy);
                        }
                    }
                }
                //мини вид v
                for(int i = 0; i<Main2Activity.FourthChartY0.length-1;i++){
                    int startX,startY,stopX,stopY;
                    startY = (int)(y(1350)-(Long.valueOf(Main2Activity.FourthChartY0[i])*y(120)/followersmaxMinimap));
                    startX = (int)((Long.valueOf(Main2Activity.FourthChartX[i])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    stopY = (y(1350));
                    stopX = (int)((Long.valueOf(Main2Activity.FourthChartX[i+1])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    canvas.drawRect(startX,startY,stopX,stopY,c1);
                }
                //мини вид^
                for(int i = 0;i<=5;i++){
                    values[i]=0 + (int) (followersmax/6)*i;
                    dates[i] = datemin +(date/6)*i;
                    canvas.drawText(String.valueOf(values[i]),x(90),y(1130)-(y(166)*i)-y(20),textPaint);
                    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd", Locale.ENGLISH);
                    Date d = new Date(dates[i]);
                    canvas.drawText(sdf.format(d),x(90)+x(900)/6*i,y(1170),textPaint);
                }

            }//FourthChart
            else if(fifth==true){
                datemaxMinimap = Max(Main2Activity.FifthChartX);
                dateminMinimap = Min(Main2Activity.FifthChartX);
                followersmaxMinimap = 100;
                followersminMinimap = 0;
                followersMinimap = followersmaxMinimap - followersminMinimap;
                dateMinimap = datemaxMinimap - dateminMinimap;

                //datemax = Max(Main2Activity.FifthChartX);
                //datemin = Min(Main2Activity.FifthChartX);
                //followersmax = Max(Main2Activity.FifthChartY0, Main2Activity.FifthChartY1, Main2Activity.FifthChartY2, Main2Activity.FifthChartY3);
                //followersmin = Min(Main2Activity.FifthChartY0, Main2Activity.FifthChartY1, Main2Activity.FifthChartY2, Main2Activity.FifthChartY3);

                //followers = followersmax - followersmin;
                //date = datemax - datemin;

                //Масштаб Y= 1000 / followers;
                //Масштаб X = 900 / date;
                c1.setColor(getResources().getColor(R.color.color5Y0));
                c2.setColor(getResources().getColor(R.color.color5Y1));
                c3.setColor(getResources().getColor(R.color.color5Y2));
                c4.setColor(getResources().getColor(R.color.color5Y3));
                c5.setColor(getResources().getColor(R.color.color5Y4));
                c6.setColor(getResources().getColor(R.color.color5Y5));

                differenceStart=Math.abs(x(90)-endRect);
                differenceStop=(x(990)-startRect);
                float startCoef = differenceStart/(x(900));
                float endCoef = differenceStop/(x(1000));

                Y0 = new String[Main2Activity.FifthChartY0.length-(int)(Main2Activity.FifthChartY0.length*endCoef)-(int)(Main2Activity.FifthChartY0.length*startCoef)];
                Y1 = new String[Y0.length];
                Y2 = new String[Y0.length];
                Y3 = new String[Y0.length];
                Y4 = new String[Y0.length];
                Y5 = new String[Y0.length];
                suma = new int[Y0.length];
                X = new String[Y0.length];
                MmY0 = new String[Main2Activity.FifthChartY0.length];
                MmY1 = new String[Main2Activity.FifthChartY0.length];
                MmY2 = new String[Main2Activity.FifthChartY0.length];
                MmY3 = new String[Main2Activity.FifthChartY0.length];
                MmY4 = new String[Main2Activity.FifthChartY0.length];
                MmY5 = new String[Main2Activity.FifthChartY0.length];


                a=0;
                int starter = (int) (Main2Activity.FifthChartY0.length * startCoef);

                for(int i = 0;i<Main2Activity.FifthChartY0.length;i++){
                    MmY0[i]=Main2Activity.FifthChartY0[i];
                    MmY1[i]=Main2Activity.FifthChartY1[i];
                    MmY2[i]=Main2Activity.FifthChartY2[i];
                    MmY3[i]=Main2Activity.FifthChartY3[i];
                    MmY4[i]=Main2Activity.FifthChartY4[i];
                    MmY5[i]=Main2Activity.FifthChartY5[i];
                }

                for(int i = 0+starter;i<Main2Activity.FifthChartY0.length-(int)((Main2Activity.FifthChartY0.length*endCoef));i++){
                    Y0[a]=Main2Activity.FifthChartY0[i];
                    Y1[a]=Main2Activity.FifthChartY1[i];
                    Y2[a]=Main2Activity.FifthChartY2[i];
                    Y3[a]=Main2Activity.FifthChartY3[i];
                    Y4[a]=Main2Activity.FifthChartY4[i];
                    Y5[a]=Main2Activity.FifthChartY5[i];
                    X[a] =Main2Activity.FifthChartX[i];
                    a=a+1;
                }
                if(!charty0){
                    for(int i=0;i<Y0.length;i++){
                        Y0[i]="0";
                    }
                    for(int i=0;i<MmY0.length;i++) {
                        MmY0[i] = "0";
                    }
                }
                if(!charty1){
                    for(int i=0;i<Y1.length;i++){
                        Y1[i]="0";
                    }
                    for(int i=0;i<MmY0.length;i++) {
                        MmY1[i] = "0";
                    }
                }
                if(!charty2){
                    for(int i=0;i<Y2.length;i++){
                        Y2[i]="0";
                    }
                    for(int i=0;i<MmY0.length;i++) {
                        MmY2[i] = "0";
                    }
                }
                if(!charty3){
                    for(int i=0;i<Y3.length;i++){
                        Y3[i]="0";
                    }
                    for(int i=0;i<MmY0.length;i++) {
                        MmY3[i] = "0";
                    }
                }
                if(!charty4){
                    for(int i=0;i<Y4.length;i++){
                        Y4[i]="0";
                    }
                    for(int i=0;i<MmY0.length;i++) {
                        MmY4[i] = "0";
                    }
                }
                if(!charty5){
                    for(int i=0;i<Y5.length;i++){
                        Y5[i]="0";
                    }
                    for(int i=0;i<MmY0.length;i++) {
                        MmY5[i] = "0";
                    }
                }
                for(int i = 0;i<MmY0.length;i++) {
                    int summa = Integer.valueOf(MmY0[i])+Integer.valueOf(MmY1[i])+Integer.valueOf(MmY2[i])+Integer.valueOf(MmY3[i])+Integer.valueOf(MmY4[i])+Integer.valueOf(MmY5[i]);
                    MmY0[i]=String.valueOf((Float.valueOf(MmY0[i])/summa)*100);
                    MmY1[i]=String.valueOf((Float.valueOf(MmY1[i])/summa)*100);
                    MmY2[i]=String.valueOf((Float.valueOf(MmY2[i])/summa)*100);
                    MmY3[i]=String.valueOf((Float.valueOf(MmY3[i])/summa)*100);
                    MmY4[i]=String.valueOf((Float.valueOf(MmY4[i])/summa)*100);
                    MmY5[i]=String.valueOf((Float.valueOf(MmY5[i])/summa)*100);
                }
                for(int i = 0;i<Y0.length;i++){
                    int summa = Integer.valueOf(Y0[i])+Integer.valueOf(Y1[i])+Integer.valueOf(Y2[i])+Integer.valueOf(Y3[i])+Integer.valueOf(Y4[i])+Integer.valueOf(Y5[i]);
                    suma[i]=summa;
                    Y0[i]=String.valueOf((Float.valueOf(Y0[i])/summa)*100);
                    Y1[i]=String.valueOf((Float.valueOf(Y1[i])/summa)*100);
                    Y2[i]=String.valueOf((Float.valueOf(Y2[i])/summa)*100);
                    Y3[i]=String.valueOf((Float.valueOf(Y3[i])/summa)*100);
                    Y4[i]=String.valueOf((Float.valueOf(Y4[i])/summa)*100);
                    Y5[i]=String.valueOf((Float.valueOf(Y5[i])/summa)*100);
                }

                datemax = Max(X);
                datemin = Min(X);
                canvas.drawText("Fruits",x(100),y(100),title);
                SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM YYYY", Locale.ENGLISH);
                Date startDate = new Date(datemin);
                Date stopDate = new Date(datemax);
                canvas.drawText(formatter.format(startDate)+" - "+formatter.format(stopDate),x(990),y(100),period);
                followersmax = 100;
                followersmin = 0;

                followers = followersmax - followersmin;
                date = datemax - datemin;

                Point a = new Point(100, 100);
                Point b = new Point(200, 100);
                Point c = new Point(200, 200);
                Point dp = new Point(100, 200);


                for (int i = 0; i < Y0.length - 1; i++) {
                        int startX, startY, stopX, stopY;

                            startY = (int) (+y(1130) - (Float.valueOf(Y0[i]) * y(1000) / followersmax));
                            startX = (int) ((Long.valueOf(X[i]) - datemin) * x(900) / date) + x(90);
                            stopY = (int) (+y(1130) - (Float.valueOf(Y0[i + 1]) * y(1000) / followersmax));
                            stopX = (int) ((Long.valueOf(X[i + 1]) - datemin) * x(900) / date) + x(90);
                            a.x=startX;
                            a.y=y(1130);
                            b.x=startX;
                            b.y=startY;
                            dp.x=stopX;
                            dp.y=y(1130);
                            c.x=stopX;
                            c.y=stopY;
                            path = new Path();
                            path.setFillType(Path.FillType.EVEN_ODD);
                            path.lineTo(a.x,a.y);
                            path.lineTo(b.x,b.y);
                            path.lineTo(c.x,c.y);
                            path.lineTo(dp.x,dp.y);
                            path.lineTo(a.x,a.y);
                            path.close();
                            canvas.drawPath(path, c1);

                            startY = (int) (+y(1130) - (Float.valueOf(Y1[i]) * y(1000) / followersmax));
                            startX = (int) ((Long.valueOf(X[i]) - datemin) * x(900) / date) + x(90);
                            stopY = (int) (+y(1130) - (Float.valueOf(Y1[i + 1]) * y(1000) / followersmax));
                            stopX = (int) ((Long.valueOf(X[i + 1]) - datemin) * x(900) / date) + x(90);
                    a.x=startX;
                    a.y=y(1130)-(y(1130)-b.y);
                    b.x=startX;
                    b.y=(int)Main2Activity.clamp((startY-(y(1130)-a.y)),y(30),y(1130));
                    dp.x=stopX;
                    dp.y=y(1130)-(y(1130)-c.y);
                    c.x=stopX;
                    c.y=(int)Main2Activity.clamp((stopY-(y(1130)-dp.y)),y(30),y(1130));
                            path = new Path();
                            path.setFillType(Path.FillType.EVEN_ODD);
                            path.lineTo(a.x,a.y);
                            path.lineTo(b.x,b.y);
                            path.lineTo(c.x,c.y);
                            path.lineTo(dp.x,dp.y);
                            path.lineTo(a.x,a.y);
                            path.close();
                            canvas.drawPath(path, c2);

                            startY = (int) (+y(1130) - (Float.valueOf(Y2[i]) * y(1000) / followersmax));
                            startX = (int) ((Long.valueOf(X[i]) - datemin) * x(900) / date) + x(90);
                            stopY = (int) (+y(1130) - (Float.valueOf(Y2[i + 1]) * y(1000) / followersmax));
                            stopX = (int) ((Long.valueOf(X[i + 1]) - datemin) * x(900) / date) + x(90);
                    a.x=startX;
                    a.y=y(1130)-(y(1130)-b.y);
                    b.x=startX;
                    b.y=(int)Main2Activity.clamp((startY-(y(1130)-a.y)),y(30),y(1130));
                    dp.x=stopX;
                    dp.y=y(1130)-(y(1130)-c.y);
                    c.x=stopX;
                    c.y=(int)Main2Activity.clamp((stopY-(y(1130)-dp.y)),y(30),y(1130));
                    path = new Path();
                            path.setFillType(Path.FillType.EVEN_ODD);
                            path.lineTo(a.x,a.y);
                            path.lineTo(b.x,b.y);
                            path.lineTo(c.x,c.y);
                            path.lineTo(dp.x,dp.y);
                            path.lineTo(a.x,a.y);
                            path.close();
                            canvas.drawPath(path, c3);

                            startY = (int) (+y(1130) - (Float.valueOf(Y3[i]) * y(1000) / followersmax));
                            startX = (int) ((Long.valueOf(X[i]) - datemin) * x(900) / date) + x(90);
                            stopY = (int) (+y(1130) - (Float.valueOf(Y3[i + 1]) * y(1000) / followersmax));
                            stopX = (int) ((Long.valueOf(X[i + 1]) - datemin) * x(900) / date) + x(90);
                    a.x=startX;
                    a.y=y(1130)-(y(1130)-b.y);
                    b.x=startX;
                    b.y=(int)Main2Activity.clamp((startY-(y(1130)-a.y)),y(30),y(1130));
                    dp.x=stopX;
                    dp.y=y(1130)-(y(1130)-c.y);
                    c.x=stopX;
                    c.y=(int)Main2Activity.clamp((stopY-(y(1130)-dp.y)),y(30),y(1130));
                    path = new Path();
                            path.setFillType(Path.FillType.EVEN_ODD);
                            path.lineTo(a.x,a.y);
                            path.lineTo(b.x,b.y);
                            path.lineTo(c.x,c.y);
                            path.lineTo(dp.x,dp.y);
                            path.lineTo(a.x,a.y);
                            path.close();
                            canvas.drawPath(path, c4);

                            startY = (int) (+y(1130) - (Float.valueOf(Y4[i]) * y(1000) / followersmax));
                            startX = (int) ((Long.valueOf(X[i]) - datemin) * x(900) / date) + x(90);
                            stopY = (int) (+y(1130) - (Float.valueOf(Y4[i + 1]) * y(1000) / followersmax));
                            stopX = (int) ((Long.valueOf(X[i + 1]) - datemin) * x(900) / date) + x(90);
                    a.x=startX;
                    a.y=y(1130)-(y(1130)-b.y);
                    b.x=startX;
                    b.y=(int)Main2Activity.clamp((startY-(y(1130)-a.y)),y(30),y(1130));
                    dp.x=stopX;
                    dp.y=y(1130)-(y(1130)-c.y);
                    c.x=stopX;
                    c.y=(int)Main2Activity.clamp((stopY-(y(1130)-dp.y)),y(30),y(1130));
                    path = new Path();
                            path.setFillType(Path.FillType.EVEN_ODD);
                            path.lineTo(a.x,a.y);
                            path.lineTo(b.x,b.y);
                            path.lineTo(c.x,c.y);
                            path.lineTo(dp.x,dp.y);
                            path.lineTo(a.x,a.y);
                            path.close();
                            canvas.drawPath(path, c5);

                            startY = (int) (+y(1130) - (Float.valueOf(Y5[i]) * y(1000) / followersmax));
                            startX = (int) ((Long.valueOf(X[i]) - datemin) * x(900) / date) + x(90);
                            stopY = (int) (+y(1130) - (Float.valueOf(Y5[i + 1]) * y(1000) / followersmax));
                            stopX = (int) ((Long.valueOf(X[i + 1]) - datemin) * x(900) / date) + x(90);
                    a.x=startX;
                    a.y=y(1130)-(y(1130)-b.y);
                    b.x=startX;
                    b.y=(int)Main2Activity.clamp((startY-(y(1130)-a.y)),y(30),y(1130));
                    dp.x=stopX;
                    dp.y=y(1130)-(y(1130)-c.y);
                    c.x=stopX;
                    c.y=(int)Main2Activity.clamp((stopY-(y(1130)-dp.y)),y(30),y(1130));
                    path = new Path();
                            path.setFillType(Path.FillType.EVEN_ODD);
                            path.lineTo(a.x,a.y);
                            path.lineTo(b.x,b.y);
                            path.lineTo(c.x,c.y);
                            path.lineTo(dp.x,dp.y);
                            path.lineTo(a.x,a.y);
                            path.close();
                            canvas.drawPath(path, c6);
                    }

                
                for(int i = 0; i<Main2Activity.FifthChartY0.length-1;i++){
                    int startX,startY,stopX,stopY;
                    startY = (int)(+y(1350)-(Float.valueOf(MmY0[i])*y(120)/followersmaxMinimap));
                    startX = (int)((Long.valueOf(Main2Activity.FifthChartX[i])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    stopY = (int)(+y(1350)-(Float.valueOf(MmY0[i+1])*y(120)/followersmaxMinimap));
                    stopX = (int)((Long.valueOf(Main2Activity.FifthChartX[i+1])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    a.x=startX;
                    a.y=y(1350);
                    b.x=startX;
                    b.y=startY;
                    c.x=stopX;
                    c.y=stopY;
                    dp.x=stopX;
                    dp.y=y(1350);
                    path = new Path();
                    path.setFillType(Path.FillType.EVEN_ODD);
                    path.lineTo(a.x,a.y);
                    path.lineTo(b.x,b.y);
                    path.lineTo(c.x,c.y);
                    path.lineTo(dp.x,dp.y);
                    path.lineTo(a.x,a.y);
                    path.close();
                    canvas.drawPath(path, c1);

                    startY = (int)(+y(1350)-(Float.valueOf(MmY1[i])*y(120)/followersmaxMinimap));
                    startX = (int)((Long.valueOf(Main2Activity.FifthChartX[i])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    stopY = (int)(+y(1350)-(Float.valueOf(MmY1[i+1])*y(120)/followersmaxMinimap));
                    stopX = (int)((Long.valueOf(Main2Activity.FifthChartX[i+1])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    a.x=startX;
                    a.y=y(1350)-(y(1350)-b.y);
                    b.x=startX;
                    b.y=(int)Main2Activity.clamp((startY-(y(1350)-a.y)),y(1130),y(1350));
                    dp.x=stopX;
                    dp.y=y(1350)-(y(1350)-c.y);
                    c.x=stopX;
                    c.y=(int)Main2Activity.clamp((stopY-(y(1350)-dp.y)),y(1130),y(1350));
                    path = new Path();
                    path.setFillType(Path.FillType.EVEN_ODD);
                    path.lineTo(a.x,a.y);
                    path.lineTo(b.x,b.y);
                    path.lineTo(c.x,c.y);
                    path.lineTo(dp.x,dp.y);
                    path.lineTo(a.x,a.y);
                    path.close();
                    canvas.drawPath(path, c2);

                    startY = (int)(+y(1350)-(Float.valueOf(MmY2[i])*y(120)/followersmaxMinimap));
                    startX = (int)((Long.valueOf(Main2Activity.FifthChartX[i])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    stopY = (int)(+y(1350)-(Float.valueOf(MmY2[i+1])*y(120)/followersmaxMinimap));
                    stopX = (int)((Long.valueOf(Main2Activity.FifthChartX[i+1])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    a.x=startX;
                    a.y=y(1350)-(y(1350)-b.y);
                    b.x=startX;
                    b.y=(int)Main2Activity.clamp((startY-(y(1350)-a.y)),y(1130),y(1350));
                    dp.x=stopX;
                    dp.y=y(1350)-(y(1350)-c.y);
                    c.x=stopX;
                    c.y=(int)Main2Activity.clamp((stopY-(y(1350)-dp.y)),y(1130),y(1350));
                    path = new Path();
                    path.setFillType(Path.FillType.EVEN_ODD);
                    path.lineTo(a.x,a.y);
                    path.lineTo(b.x,b.y);
                    path.lineTo(c.x,c.y);
                    path.lineTo(dp.x,dp.y);
                    path.lineTo(a.x,a.y);
                    path.close();
                    canvas.drawPath(path, c3);

                    startY = (int)(+y(1350)-(Float.valueOf(MmY3[i])*y(120)/followersmaxMinimap));
                    startX = (int)((Long.valueOf(Main2Activity.FifthChartX[i])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    stopY = (int)(+y(1350)-(Float.valueOf(MmY3[i+1])*y(120)/followersmaxMinimap));
                    stopX = (int)((Long.valueOf(Main2Activity.FifthChartX[i+1])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    a.x=startX;
                    a.y=y(1350)-(y(1350)-b.y);
                    b.x=startX;
                    b.y=(int)Main2Activity.clamp((startY-(y(1350)-a.y)),y(1130),y(1350));
                    dp.x=stopX;
                    dp.y=y(1350)-(y(1350)-c.y);
                    c.x=stopX;
                    c.y=(int)Main2Activity.clamp((stopY-(y(1350)-dp.y)),y(1130),y(1350));
                    path = new Path();
                    path.setFillType(Path.FillType.EVEN_ODD);
                    path.lineTo(a.x,a.y);
                    path.lineTo(b.x,b.y);
                    path.lineTo(c.x,c.y);
                    path.lineTo(dp.x,dp.y);
                    path.lineTo(a.x,a.y);
                    path.close();
                    canvas.drawPath(path, c4);

                    startY = (int)(+y(1350)-(Float.valueOf(MmY4[i])*y(120)/followersmaxMinimap));
                    startX = (int)((Long.valueOf(Main2Activity.FifthChartX[i])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    stopY = (int)(+y(1350)-(Float.valueOf(MmY4[i+1])*y(120)/followersmaxMinimap));
                    stopX = (int)((Long.valueOf(Main2Activity.FifthChartX[i+1])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    a.x=startX;
                    a.y=y(1350)-(y(1350)-b.y);
                    b.x=startX;
                    b.y=(int)Main2Activity.clamp((startY-(y(1350)-a.y)),y(1130),y(1350));
                    dp.x=stopX;
                    dp.y=y(1350)-(y(1350)-c.y);
                    c.x=stopX;
                    c.y=(int)Main2Activity.clamp((stopY-(y(1350)-dp.y)),y(1130),y(1350));
                    path = new Path();
                    path.setFillType(Path.FillType.EVEN_ODD);
                    path.lineTo(a.x,a.y);
                    path.lineTo(b.x,b.y);
                    path.lineTo(c.x,c.y);
                    path.lineTo(dp.x,dp.y);
                    path.lineTo(a.x,a.y);
                    path.close();
                    canvas.drawPath(path, c5);

                    startY = (int)(+y(1350)-(Float.valueOf(MmY5[i])*y(120)/followersmaxMinimap));
                    startX = (int)((Long.valueOf(Main2Activity.FifthChartX[i])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    stopY = (int)(+y(1350)-(Float.valueOf(MmY5[i+1])*y(120)/followersmaxMinimap));
                    stopX = (int)((Long.valueOf(Main2Activity.FifthChartX[i+1])-dateminMinimap)*x(900)/dateMinimap)+x(90);
                    a.x=startX;
                    a.y=y(1350)-(y(1350)-b.y);
                    b.x=startX;
                    b.y=(int)Main2Activity.clamp((startY-(y(1350)-a.y)),y(1130),y(1350));
                    dp.x=stopX;
                    dp.y=y(1350)-(y(1350)-c.y);
                    c.x=stopX;
                    c.y=(int)Main2Activity.clamp((stopY-(y(1350)-dp.y)),y(1130),y(1350));
                    path = new Path();
                    path.setFillType(Path.FillType.EVEN_ODD);
                    path.lineTo(a.x,a.y);
                    path.lineTo(b.x,b.y);
                    path.lineTo(c.x,c.y);
                    path.lineTo(dp.x,dp.y);
                    path.lineTo(a.x,a.y);
                    path.close();
                    canvas.drawPath(path, c6);
                }

                
                
                for(int i = 0;i<=5;i++){
                    dates[i] = datemin +(date/6)*i;
                    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd", Locale.ENGLISH);
                    Date d = new Date(dates[i]);
                    canvas.drawText(sdf.format(d),x(90)+x(900)/6*i,y(1170),textPaint);
                }
                for(int i = 0;i<5;i++){
                    values[i]=0 + (int) (followersmax/4)*i;
                    canvas.drawText(String.valueOf(values[i]),x(90),y(1130)-(y(250)*i)-y(20),textPaint);
                }
            }//FifthChart

            if(fifth) {
                canvas.drawLine(x(90), y(130), x(990), y(130), p);
                canvas.drawLine(x(90), y(380), x(990), y(380), p);
                canvas.drawLine(x(90), y(630), x(990), y(630), p);
                canvas.drawLine(x(90), y(880), x(990), y(880), p);
                canvas.drawLine(x(90), y(1130), x(990), y(1130), p);
            }//draws lines for fifth Chart
            else{
                canvas.drawLine(x(90), y(300), x(990), y(300), p);
                canvas.drawLine(x(90), y(466), x(990), y(466), p);
                canvas.drawLine(x(90), y(632), x(990), y(632), p);
                canvas.drawLine(x(90), y(798), x(990), y(798), p);
                canvas.drawLine(x(90), y(964), x(990), y(964), p);
                canvas.drawLine(x(90), y(1130), x(990), y(1130), p);
            }//draws lines for else Charts

            canvas.drawRect(rectLeft, r);
            canvas.drawLine((int)Scale(Float.valueOf(endRectDraw)),y(1350),(int)Scale(Float.valueOf(startRectDraw)),y(1350),rm);
            canvas.drawLine((int)Scale(Float.valueOf(endRectDraw)),y(1230),(int)Scale(Float.valueOf(startRectDraw)),y(1230),rm);
            canvas.drawRect(rectRight,r);


            if(selectedPosition!=0){


                int x1 =selectedPosition;
                arrayItemN = (int)Main2Activity.clamp((Y0.length*(x1-x(90))/x(900)),0,Y0.length-1);
                SimpleDateFormat sdf = new SimpleDateFormat("E,dd MMM YYYY", Locale.ENGLISH);
                long l = Long.valueOf(X[arrayItemN]);
                Date d = new Date(l);

                TextPaint data = new TextPaint();
                data.setTextSize(x(50));
                data.setTextAlign(Paint.Align.LEFT);
                data.setTypeface(Typeface.create("Tw Cen MT", Typeface.BOLD));

                TextPaint dataRight = new TextPaint();
                dataRight.setTextSize(x(50));
                dataRight.setTextAlign(Paint.Align.RIGHT);
                dataRight.setTypeface(Typeface.create("Tw Cen MT", Typeface.BOLD));

                if(Main2Activity.night_mode){data.setColor(Color.WHITE); dataRight.setColor(Color.WHITE);}
                else {data.setColor(Color.BLACK); dataRight.setColor(Color.BLACK);}

                TextPaint fy0Text;
                TextPaint fy1Text;
                TextPaint fy2Text;
                TextPaint fy3Text;
                TextPaint fy4Text;
                TextPaint fy5Text;
                TextPaint fy6Text;
                TextPaint y0Text;
                TextPaint y1Text;

                TextPaint fy0TextN;
                TextPaint y0TextN;
                TextPaint y1TextN;


                if(first) {
                    canvas.drawLine(selectedPosition,y(130),selectedPosition,y(1130),p);

                    y0Text = new TextPaint(Paint.ANTI_ALIAS_FLAG);
                    y0Text.setTextSize(x(50));
                    y0Text.setTextAlign(Paint.Align.RIGHT);
                    y0Text.setColor(getResources().getColor(R.color.colorY0));
                    y0Text.setTypeface(Typeface.create("Tw Cen MT", Typeface.BOLD));

                    y1Text = new TextPaint(Paint.ANTI_ALIAS_FLAG);
                    y1Text.setTextSize(x(50));
                    y1Text.setTextAlign(Paint.Align.RIGHT);
                    y1Text.setTypeface(Typeface.create("Tw Cen MT", Typeface.BOLD));
                    y1Text.setColor(getResources().getColor(R.color.colorY1));

                    y0TextN=new TextPaint(Paint.ANTI_ALIAS_FLAG);
                    y0TextN.setTextSize(x(50));
                    y0TextN.setTextAlign(Paint.Align.LEFT);
                    y0TextN.setTypeface(Typeface.create("Tw Cen MT", Typeface.NORMAL));
                    y0TextN.setColor(Color.BLACK);
                    
                    y1TextN=new TextPaint(Paint.ANTI_ALIAS_FLAG);
                    y1TextN.setTextAlign(Paint.Align.LEFT);
                    y1TextN.setTextSize(x(50));
                    y1TextN.setTypeface(Typeface.create("Tw Cen MT", Typeface.NORMAL));
                    y1TextN.setColor(Color.BLACK);


                    int y1 = (int) (y(1130) - (Long.valueOf(Y0[arrayItemN])-followersmin) * y(1000) / followers);
                    int y2 = (int)(y(1130) - (Long.valueOf(Y1[arrayItemN])-followersmin) * y(1000) / followers);
                    Paint e1,f1,e2,f2;
                    f1 = new Paint(Paint.ANTI_ALIAS_FLAG);
                    if(Main2Activity.night_mode)f1.setColor(getResources().getColor(R.color.colorNightModeLayout));
                    else f1.setColor(Color.WHITE);                    
                    e1 = new Paint(Paint.ANTI_ALIAS_FLAG);
                    e1.setStyle(Paint.Style.STROKE);
                    e1.setStrokeWidth(10);
                    e1.setColor(getResources().getColor(R.color.colorY0));
                    f2 = new Paint(Paint.ANTI_ALIAS_FLAG);
                    if(Main2Activity.night_mode)f2.setColor(getResources().getColor(R.color.colorNightModeLayout));
                    else f2.setColor(Color.WHITE);                    
                    e2 = new Paint(Paint.ANTI_ALIAS_FLAG);
                    e2.setStyle(Paint.Style.STROKE);
                    e2.setStrokeWidth(10);
                    e2.setColor(getResources().getColor(R.color.colorY1));
                    canvas.drawCircle(x1,y1,18,e1);//круг цвета background
                    canvas.drawCircle(x1,y1,18,f1);//круг цвета линии х
                    canvas.drawCircle(x1,y2,18,e2);//круг цвета background
                    canvas.drawCircle(x1,y2,18,f2);//круг цвета линии х

                    if(selectedPosition>x(580)) {
                        RectF info = new RectF(selectedPosition - x(570), y(90), selectedPosition , y(360));
                        Paint forRect = new Paint();
                        Paint forRectStroke = new Paint();
                        forRectStroke.setStyle(Paint.Style.STROKE);
                        forRectStroke.setColor(Color.GRAY);
                        if (Main2Activity.night_mode) {
                            forRect.setColor(getResources().getColor(R.color.colorNightModeInfoTable));
                            canvas.drawRoundRect(info, 18, 18, forRect);
                            y0TextN.setColor(Color.WHITE);
                            y1TextN.setColor(Color.WHITE);
                        }
                        else {
                            forRect.setColor(Color.WHITE);
                            canvas.drawRoundRect(info, 18, 18, forRect);
                            canvas.drawRoundRect(info, 18, 18, forRectStroke);
                            y0TextN.setColor(Color.BLACK);
                            y1TextN.setColor(Color.BLACK);

                        }
                    }
                    else {
                        RectF info = new RectF(selectedPosition - x(70), y(90), selectedPosition + x(500), y(360));
                        Paint forRect = new Paint();
                        Paint forRectStroke = new Paint();
                        forRectStroke.setStyle(Paint.Style.STROKE);
                        forRectStroke.setColor(Color.GRAY);
                        if (Main2Activity.night_mode) {
                            forRect.setColor(getResources().getColor(R.color.colorNightModeInfoTable));
                            canvas.drawRoundRect(info, 18, 18, forRect);
                            y0TextN.setColor(Color.WHITE);
                            y1TextN.setColor(Color.WHITE);
                        }
                        else {
                            forRect.setColor(Color.WHITE);
                            canvas.drawRoundRect(info, 18, 18, forRect);
                            canvas.drawRoundRect(info, 18, 18, forRectStroke);
                            y0TextN.setColor(Color.BLACK);
                            y1TextN.setColor(Color.BLACK);

                        }

                    }
                    if(selectedPosition>x(580)) {
                        canvas.drawText(sdf.format(d), selectedPosition - x(540), y(150), data);
                        canvas.drawLine(selectedPosition-x(35),y(115),selectedPosition-x(15),y(130),gray);
                        canvas.drawLine(selectedPosition-x(15),y(130),selectedPosition-x(35),y(145),gray);
                        if(charty0)canvas.drawText(Y0[arrayItemN], selectedPosition - x(15), y(250), y0Text);
                        if(charty1)canvas.drawText(Y1[arrayItemN], selectedPosition - x(15), y(330), y1Text);
                        if(charty0)canvas.drawText("Joined", selectedPosition - x(540), y(250), y0TextN);
                        if(charty1)canvas.drawText("Left", selectedPosition - x(540), y(330), y1TextN);
                    }
                    else{
                        canvas.drawText(sdf.format(d), selectedPosition - x(30), y(150), data);
                        canvas.drawLine(selectedPosition+x(460),y(115),selectedPosition+x(480),y(130),gray);
                        canvas.drawLine(selectedPosition+x(480),y(130),selectedPosition+x(460),y(145),gray);
                        if(charty0)canvas.drawText(Y0[arrayItemN], selectedPosition + x(480), y(250), y0Text);
                        if(charty1)canvas.drawText(Y1[arrayItemN], selectedPosition + x(480), y(330), y1Text);
                        if(charty0)canvas.drawText("Joined", selectedPosition - x(30), y(250), y0TextN);
                        if(charty1)canvas.drawText("Left", selectedPosition - x(30), y(330), y1TextN);
                    }

                }
                else if(second) {
                    canvas.drawLine(selectedPosition,y(130),selectedPosition,y(1130),p);
                    y0Text = new TextPaint(Paint.ANTI_ALIAS_FLAG);
                    y0Text.setTextSize(x(50));
                    y0Text.setTextAlign(Paint.Align.RIGHT);
                    y0Text.setColor(getResources().getColor(R.color.color2Y0));
                    y0Text.setTypeface(Typeface.create("Tw Cen MT", Typeface.BOLD));

                    y1Text = new TextPaint(Paint.ANTI_ALIAS_FLAG);
                    y1Text.setTextSize(x(50));
                    y1Text.setTextAlign(Paint.Align.RIGHT);
                    y1Text.setTypeface(Typeface.create("Tw Cen MT", Typeface.BOLD));
                    y1Text.setColor(getResources().getColor(R.color.color2Y1));

                    y0TextN=new TextPaint(Paint.ANTI_ALIAS_FLAG);
                    y0TextN.setTextSize(x(50));
                    y0TextN.setTextAlign(Paint.Align.LEFT);
                    y0TextN.setTypeface(Typeface.create("Tw Cen MT", Typeface.NORMAL));
                    y0TextN.setColor(Color.BLACK);

                    y1TextN=new TextPaint(Paint.ANTI_ALIAS_FLAG);
                    y1TextN.setTextAlign(Paint.Align.LEFT);
                    y1TextN.setTextSize(x(50));
                    y1TextN.setTypeface(Typeface.create("Tw Cen MT", Typeface.NORMAL));
                    y1TextN.setColor(Color.BLACK);


                    int y1 = (int) (y(1130) - (Long.valueOf(Y0[arrayItemN])-followersmin0) * y(1000) / followers0);
                    int y2 = (int)(y(1130) - (Long.valueOf(Y1[arrayItemN])-followersmin1) * y(1000) / followers1);
                    Paint e1,f1,e2,f2;
                    f1 = new Paint(Paint.ANTI_ALIAS_FLAG);
                    if(Main2Activity.night_mode)f1.setColor(getResources().getColor(R.color.colorNightModeLayout));
                    else f1.setColor(Color.WHITE);
                    e1 = new Paint(Paint.ANTI_ALIAS_FLAG);
                    e1.setStyle(Paint.Style.STROKE);
                    e1.setStrokeWidth(10);
                    e1.setColor(getResources().getColor(R.color.color2Y0));
                    f2 = new Paint(Paint.ANTI_ALIAS_FLAG);
                    if(Main2Activity.night_mode)f2.setColor(getResources().getColor(R.color.colorNightModeLayout));
                    else f2.setColor(Color.WHITE);
                    e2 = new Paint(Paint.ANTI_ALIAS_FLAG);
                    e2.setStyle(Paint.Style.STROKE);
                    e2.setStrokeWidth(10);
                    e2.setColor(getResources().getColor(R.color.color2Y1));
                    canvas.drawCircle(x1,y1,18,e1);//круг цвета background
                    canvas.drawCircle(x1,y1,18,f1);//круг цвета линии х
                    canvas.drawCircle(x1,y2,18,e2);//круг цвета background
                    canvas.drawCircle(x1,y2,18,f2);//круг цвета линии х

                    if(selectedPosition>x(580)) {
                        RectF info = new RectF(selectedPosition - x(570), y(90), selectedPosition , y(360));                        Paint forRect = new Paint();
                        Paint forRectStroke = new Paint();
                        forRectStroke.setStyle(Paint.Style.STROKE);
                        forRectStroke.setColor(Color.GRAY);
                        if (Main2Activity.night_mode) {
                            forRect.setColor(getResources().getColor(R.color.colorNightModeInfoTable));
                            canvas.drawRoundRect(info, 18, 18, forRect);
                            y0TextN.setColor(Color.WHITE);
                            y1TextN.setColor(Color.WHITE);
                        }
                        else {
                            forRect.setColor(Color.WHITE);
                            canvas.drawRoundRect(info, 18, 18, forRect);
                            canvas.drawRoundRect(info, 18, 18, forRectStroke);
                            y0TextN.setColor(Color.BLACK);
                            y1TextN.setColor(Color.BLACK);

                        }
                    }
                    else {
                        RectF info = new RectF(selectedPosition - x(70), y(90), selectedPosition + x(500), y(360));
                        Paint forRect = new Paint();
                        Paint forRectStroke = new Paint();
                        forRectStroke.setStyle(Paint.Style.STROKE);
                        forRectStroke.setColor(Color.GRAY);
                        if (Main2Activity.night_mode) {
                            forRect.setColor(getResources().getColor(R.color.colorNightModeInfoTable));
                            canvas.drawRoundRect(info, 18, 18, forRect);
                            y0TextN.setColor(Color.WHITE);
                            y1TextN.setColor(Color.WHITE);
                        }
                        else {
                            forRect.setColor(Color.WHITE);
                            canvas.drawRoundRect(info, 18, 18, forRect);
                            canvas.drawRoundRect(info, 18, 18, forRectStroke);
                            y0TextN.setColor(Color.BLACK);
                            y1TextN.setColor(Color.BLACK);

                        }

                    }
                    if(selectedPosition>x(580)) {
                        canvas.drawText(sdf.format(d), selectedPosition - x(540), y(150), data);
                        canvas.drawLine(selectedPosition-x(35),y(115),selectedPosition-x(15),y(130),gray);
                        canvas.drawLine(selectedPosition-x(15),y(130),selectedPosition-x(35),y(145),gray);
                        if(charty0)canvas.drawText(Y0[arrayItemN], selectedPosition - x(15), y(250), y0Text);
                        if(charty1)canvas.drawText(Y1[arrayItemN], selectedPosition - x(15), y(330), y1Text);
                        if(charty0)canvas.drawText("Views", selectedPosition - x(540), y(250), y0TextN);
                        if(charty1)canvas.drawText("Shares", selectedPosition - x(540), y(330), y1TextN);
                    }
                    else {
                        canvas.drawText(sdf.format(d), selectedPosition - x(30), y(150), data);
                        canvas.drawLine(selectedPosition + x(460), y(115), selectedPosition + x(480), y(130), gray);
                        canvas.drawLine(selectedPosition + x(480), y(130), selectedPosition + x(460), y(145), gray);
                        if (charty0)
                            canvas.drawText(Y0[arrayItemN], selectedPosition + x(480), y(250), y0Text);
                        if (charty1)
                            canvas.drawText(Y1[arrayItemN], selectedPosition + x(480), y(330), y1Text);
                        if (charty0)
                            canvas.drawText("Views", selectedPosition - x(30), y(250), y0TextN);
                        if (charty1)
                            canvas.drawText("Shares", selectedPosition - x(30), y(330), y1TextN);
                    }


                }
                else if(third){

                    fy0Text = new TextPaint(Paint.ANTI_ALIAS_FLAG);
                    fy0Text.setTextSize(x(50));
                    fy0Text.setTextAlign(Paint.Align.RIGHT);
                    fy0Text.setColor(getResources().getColor(R.color.color3Y0));
                    fy0Text.setTypeface(Typeface.create("Tw Cen MT", Typeface.BOLD));

                    fy1Text = new TextPaint(Paint.ANTI_ALIAS_FLAG);
                    fy1Text.setTextSize(x(50));
                    fy1Text.setTextAlign(Paint.Align.RIGHT);
                    fy1Text.setTypeface(Typeface.create("Tw Cen MT", Typeface.BOLD));
                    fy1Text.setColor(getResources().getColor(R.color.color3Y1));

                    fy2Text = new TextPaint(Paint.ANTI_ALIAS_FLAG);
                    fy2Text.setTextSize(x(50));
                    fy2Text.setTextAlign(Paint.Align.RIGHT);
                    fy2Text.setColor(getResources().getColor(R.color.color3Y2));
                    fy2Text.setTypeface(Typeface.create("Tw Cen MT", Typeface.BOLD));

                    fy3Text = new TextPaint(Paint.ANTI_ALIAS_FLAG);
                    fy3Text.setTextSize(x(50));
                    fy3Text.setTextAlign(Paint.Align.RIGHT);
                    fy3Text.setTypeface(Typeface.create("Tw Cen MT", Typeface.BOLD));
                    fy3Text.setColor(getResources().getColor(R.color.color3Y3));

                    fy4Text = new TextPaint(Paint.ANTI_ALIAS_FLAG);
                    fy4Text.setTextSize(x(50));
                    fy4Text.setTextAlign(Paint.Align.RIGHT);
                    fy4Text.setTypeface(Typeface.create("Tw Cen MT", Typeface.BOLD));
                    fy4Text.setColor(getResources().getColor(R.color.color3Y4));

                    fy5Text = new TextPaint(Paint.ANTI_ALIAS_FLAG);
                    fy5Text.setTextSize(x(50));
                    fy5Text.setTextAlign(Paint.Align.RIGHT);
                    fy5Text.setTypeface(Typeface.create("Tw Cen MT", Typeface.BOLD));
                    fy5Text.setColor(getResources().getColor(R.color.color3Y5));

                    fy6Text = new TextPaint(Paint.ANTI_ALIAS_FLAG);
                    fy6Text.setTextSize(x(50));
                    fy6Text.setTextAlign(Paint.Align.RIGHT);
                    fy6Text.setTypeface(Typeface.create("Tw Cen MT", Typeface.BOLD));
                    fy6Text.setColor(getResources().getColor(R.color.color3Y6));

                    fy0TextN=new TextPaint(Paint.ANTI_ALIAS_FLAG);
                    fy0TextN.setTextSize(x(50));
                    fy0TextN.setTextAlign(Paint.Align.LEFT);
                    fy0TextN.setTypeface(Typeface.create("Tw Cen MT", Typeface.NORMAL));
                    fy0TextN.setColor(Color.BLACK);



                    if(selectedPosition>x(580)) {
                        RectF info = new RectF(selectedPosition - x(600), y(90), selectedPosition-x(30) , y(840));
                        Paint forRect = new Paint();
                        Paint forRectStroke = new Paint();
                        forRectStroke.setStyle(Paint.Style.STROKE);
                        forRectStroke.setColor(Color.GRAY);
                        if (Main2Activity.night_mode) {
                            forRect.setColor(getResources().getColor(R.color.colorNightModeInfoTable));
                            canvas.drawRoundRect(info, 18, 18, forRect);
                            fy0TextN.setColor(Color.WHITE);
                        }
                        else {
                            forRect.setColor(Color.WHITE);
                            canvas.drawRoundRect(info, 18, 18, forRect);
                            canvas.drawRoundRect(info, 18, 18, forRectStroke);
                            fy0TextN.setColor(Color.BLACK);
                        }
                    }
                    else {
                        RectF info = new RectF(selectedPosition+x(30), y(90), selectedPosition + x(600), y(840));
                        Paint forRect = new Paint();
                        Paint forRectStroke = new Paint();
                        forRectStroke.setStyle(Paint.Style.STROKE);
                        forRectStroke.setColor(Color.GRAY);
                        if (Main2Activity.night_mode) {
                            forRect.setColor(getResources().getColor(R.color.colorNightModeInfoTable));
                            canvas.drawRoundRect(info, 18, 18, forRect);
                            fy0TextN.setColor(Color.WHITE);
                        }
                        else {
                            forRect.setColor(Color.WHITE);
                            canvas.drawRoundRect(info, 18, 18, forRect);
                            canvas.drawRoundRect(info, 18, 18, forRectStroke);
                            fy0TextN.setColor(Color.BLACK);

                        }

                    }

                    if(selectedPosition>x(580)) {
                        canvas.drawText(sdf.format(d), selectedPosition - x(565), y(150), data);
                        canvas.drawLine(selectedPosition-x(65),y(115),selectedPosition-x(45),y(130),gray);
                        canvas.drawLine(selectedPosition-x(45),y(130),selectedPosition-x(65),y(145),gray);
                        int all=0;
                        if(charty0)canvas.drawText(Y0[arrayItemN], selectedPosition - x(45), y(250), fy0Text); all = all+Math.round(Float.valueOf(Y0[arrayItemN]));
                        if(charty1)canvas.drawText(Y1[arrayItemN], selectedPosition - x(45), y(330), fy1Text);all = all+Math.round(Float.valueOf(Y1[arrayItemN]));
                        if(charty2)canvas.drawText(Y2[arrayItemN], selectedPosition - x(45), y(410), fy2Text);all = all+Math.round(Float.valueOf(Y2[arrayItemN]));
                        if(charty3)canvas.drawText(Y3[arrayItemN], selectedPosition - x(45), y(490), fy3Text);all = all+Math.round(Float.valueOf(Y3[arrayItemN]));
                        if(charty4)canvas.drawText(Y4[arrayItemN], selectedPosition - x(45), y(570), fy4Text);all = all+Math.round(Float.valueOf(Y4[arrayItemN]));
                        if(charty5)canvas.drawText(Y5[arrayItemN], selectedPosition - x(45), y(650), fy5Text);all = all+Math.round(Float.valueOf(Y5[arrayItemN]));
                        if(charty6)canvas.drawText(Y6[arrayItemN], selectedPosition - x(45), y(730), fy6Text);all = all+Math.round(Float.valueOf(Y6[arrayItemN]));
                        canvas.drawText(String.valueOf(all), selectedPosition - x(45), y(810), dataRight);
                        if(charty0)canvas.drawText("Apples", selectedPosition - x(565), y(250), fy0TextN);
                        if(charty1)canvas.drawText("Oranges", selectedPosition - x(565), y(330), fy0TextN);
                        if(charty2)canvas.drawText("Lemons", selectedPosition - x(565), y(410), fy0TextN);
                        if(charty3)canvas.drawText("Apricots", selectedPosition - x(565), y(490), fy0TextN);
                        if(charty4)canvas.drawText("Kiwi", selectedPosition - x(565), y(570), fy0TextN);
                        if(charty5)canvas.drawText("Mango", selectedPosition - x(565), y(650), fy0TextN);
                        if(charty6)canvas.drawText("Pears", selectedPosition - x(565), y(730), fy0TextN);
                        canvas.drawText("All", selectedPosition - x(565), y(810), fy0TextN);

                    }
                    else{
                        canvas.drawText(sdf.format(d), selectedPosition + x(75), y(150), data);
                        canvas.drawLine(selectedPosition+x(560),y(115),selectedPosition+x(580),y(130),gray);
                        canvas.drawLine(selectedPosition+x(580),y(130),selectedPosition+x(560),y(145),gray);
                        int all=0;
                        if(charty0)canvas.drawText(Y0[arrayItemN], selectedPosition + x(580), y(250), fy0Text);all = all+Math.round(Float.valueOf(Y0[arrayItemN]));
                        if(charty1)canvas.drawText(Y1[arrayItemN], selectedPosition + x(580), y(330), fy1Text);all = all+Math.round(Float.valueOf(Y1[arrayItemN]));
                        if(charty2)canvas.drawText(Y2[arrayItemN], selectedPosition + x(580), y(410), fy2Text);all = all+Math.round(Float.valueOf(Y2[arrayItemN]));
                        if(charty3)canvas.drawText(Y3[arrayItemN], selectedPosition + x(580), y(490), fy3Text);all = all+Math.round(Float.valueOf(Y3[arrayItemN]));
                        if(charty4)canvas.drawText(Y4[arrayItemN], selectedPosition + x(580), y(570), fy4Text);all = all+Math.round(Float.valueOf(Y4[arrayItemN]));
                        if(charty5)canvas.drawText(Y5[arrayItemN], selectedPosition + x(580), y(650), fy5Text);all = all+Math.round(Float.valueOf(Y5[arrayItemN]));
                        if(charty6)canvas.drawText(Y6[arrayItemN], selectedPosition + x(580), y(730), fy6Text);all = all+Math.round(Float.valueOf(Y6[arrayItemN]));
                        canvas.drawText(String.valueOf(all), selectedPosition + x(580), y(810), dataRight);
                        if(charty0)canvas.drawText("Joined", selectedPosition + x(75), y(250), fy0TextN);
                        if(charty1)canvas.drawText("Left", selectedPosition + x(75), y(330), fy0TextN);
                        if(charty2)canvas.drawText("Lemons", selectedPosition + x(75), y(410), fy0TextN);
                        if(charty3)canvas.drawText("Apricots", selectedPosition + x(75), y(490), fy0TextN);
                        if(charty4)canvas.drawText("Kiwi", selectedPosition + x(75), y(570), fy0TextN);
                        if(charty5)canvas.drawText("Mango", selectedPosition + x(75), y(650), fy0TextN);
                        if(charty6)canvas.drawText("Pears", selectedPosition + x(75), y(730), fy0TextN);
                        canvas.drawText("All", selectedPosition + x(75), y(810), fy0TextN);
                    }
                }
                else if(fourth) {


                    y0Text = new TextPaint(Paint.ANTI_ALIAS_FLAG);
                    y0Text.setTextSize(x(50));
                    y0Text.setTextAlign(Paint.Align.RIGHT);
                    y0Text.setColor(getResources().getColor(R.color.color4Y0));
                    y0Text.setTypeface(Typeface.create("Tw Cen MT", Typeface.BOLD));

                    y0TextN=new TextPaint(Paint.ANTI_ALIAS_FLAG);
                    y0TextN.setTextSize(x(50));
                    y0TextN.setTextAlign(Paint.Align.LEFT);
                    y0TextN.setTypeface(Typeface.create("Tw Cen MT", Typeface.NORMAL));
                    y0TextN.setColor(Color.BLACK);




                    if(selectedPosition>x(580)) {
                        RectF info = new RectF(selectedPosition - x(570), y(90), selectedPosition , y(260));
                        Paint forRect = new Paint();
                        Paint forRectStroke = new Paint();
                        forRectStroke.setStyle(Paint.Style.STROKE);
                        forRectStroke.setColor(Color.GRAY);
                        if (Main2Activity.night_mode) {
                            forRect.setColor(getResources().getColor(R.color.colorNightModeInfoTable));
                            canvas.drawRoundRect(info, 18, 18, forRect);
                            y0TextN.setColor(Color.WHITE);
                        }
                        else {
                            forRect.setColor(Color.WHITE);
                            canvas.drawRoundRect(info, 18, 18, forRect);
                            canvas.drawRoundRect(info, 18, 18, forRectStroke);
                            y0TextN.setColor(Color.BLACK);

                        }
                    }
                    else {
                        RectF info = new RectF(selectedPosition - x(70), y(90), selectedPosition + x(500), y(260));
                        Paint forRect = new Paint();
                        Paint forRectStroke = new Paint();
                        forRectStroke.setStyle(Paint.Style.STROKE);
                        forRectStroke.setColor(Color.GRAY);
                        if (Main2Activity.night_mode) {
                            forRect.setColor(getResources().getColor(R.color.colorNightModeInfoTable));
                            canvas.drawRoundRect(info, 18, 18, forRect);
                            y0TextN.setColor(Color.WHITE);
                        }
                        else {
                            forRect.setColor(Color.WHITE);
                            canvas.drawRoundRect(info, 18, 18, forRect);
                            canvas.drawRoundRect(info, 18, 18, forRectStroke);
                            y0TextN.setColor(Color.BLACK);
                        }

                    }
                    if(selectedPosition>x(580)) {
                        canvas.drawText(sdf.format(d), selectedPosition - x(540), y(150), data);
                        canvas.drawLine(selectedPosition-x(35),y(115),selectedPosition-x(15),y(130),gray);
                        canvas.drawLine(selectedPosition-x(15),y(130),selectedPosition-x(35),y(145),gray);
                        canvas.drawText(Y0[arrayItemN], selectedPosition - x(15), y(220), y0Text);
                        canvas.drawText("Views", selectedPosition - x(540), y(220), y0TextN);
                    }
                    else{
                        canvas.drawText(sdf.format(d), selectedPosition - x(30), y(150), data);
                        canvas.drawLine(selectedPosition+x(460),y(115),selectedPosition+x(480),y(130),gray);
                        canvas.drawLine(selectedPosition+x(480),y(130),selectedPosition+x(460),y(145),gray);
                        canvas.drawText(Y0[arrayItemN], selectedPosition + x(480), y(220), y0Text);
                        canvas.drawText("Views", selectedPosition - x(30), y(220), y0TextN);
                    }
                }
                if(fifth){
                    canvas.drawLine(selectedPosition,y(130),selectedPosition,y(1130),p);
                    fy0Text = new TextPaint(Paint.ANTI_ALIAS_FLAG);
                    fy0Text.setTextSize(x(50));
                    fy0Text.setTextAlign(Paint.Align.RIGHT);
                    fy0Text.setColor(getResources().getColor(R.color.color5Y0));
                    fy0Text.setTypeface(Typeface.create("Tw Cen MT", Typeface.BOLD));

                    fy1Text = new TextPaint(Paint.ANTI_ALIAS_FLAG);
                    fy1Text.setTextSize(x(50));
                    fy1Text.setTextAlign(Paint.Align.RIGHT);
                    fy1Text.setTypeface(Typeface.create("Tw Cen MT", Typeface.BOLD));
                    fy1Text.setColor(getResources().getColor(R.color.color5Y1));

                    fy2Text = new TextPaint(Paint.ANTI_ALIAS_FLAG);
                    fy2Text.setTextSize(x(50));
                    fy2Text.setTextAlign(Paint.Align.RIGHT);
                    fy2Text.setColor(getResources().getColor(R.color.color5Y2));
                    fy2Text.setTypeface(Typeface.create("Tw Cen MT", Typeface.BOLD));

                    fy3Text = new TextPaint(Paint.ANTI_ALIAS_FLAG);
                    fy3Text.setTextSize(x(50));
                    fy3Text.setTextAlign(Paint.Align.RIGHT);
                    fy3Text.setTypeface(Typeface.create("Tw Cen MT", Typeface.BOLD));
                    fy3Text.setColor(getResources().getColor(R.color.color5Y3));

                    fy4Text = new TextPaint(Paint.ANTI_ALIAS_FLAG);
                    fy4Text.setTextSize(x(50));
                    fy4Text.setTextAlign(Paint.Align.RIGHT);
                    fy4Text.setTypeface(Typeface.create("Tw Cen MT", Typeface.BOLD));
                    fy4Text.setColor(getResources().getColor(R.color.color5Y4));

                    fy5Text = new TextPaint(Paint.ANTI_ALIAS_FLAG);
                    fy5Text.setTextSize(x(50));
                    fy5Text.setTextAlign(Paint.Align.RIGHT);
                    fy5Text.setTypeface(Typeface.create("Tw Cen MT", Typeface.BOLD));
                    fy5Text.setColor(getResources().getColor(R.color.color5Y5));

                    fy0TextN=new TextPaint(Paint.ANTI_ALIAS_FLAG);
                    fy0TextN.setTextSize(x(50));
                    fy0TextN.setTextAlign(Paint.Align.LEFT);
                    fy0TextN.setTypeface(Typeface.create("Tw Cen MT", Typeface.NORMAL));
                    fy0TextN.setColor(Color.BLACK);



                    if(selectedPosition>x(580)) {
                        RectF info = new RectF(selectedPosition - x(600), y(90), selectedPosition-x(30) , y(670));
                        Paint forRect = new Paint();
                        Paint forRectStroke = new Paint();
                        forRectStroke.setStyle(Paint.Style.STROKE);
                        forRectStroke.setColor(Color.GRAY);
                        if (Main2Activity.night_mode) {
                            forRect.setColor(getResources().getColor(R.color.colorNightModeInfoTable));
                            canvas.drawRoundRect(info, 18, 18, forRect);
                            fy0TextN.setColor(Color.WHITE);
                        }
                        else {
                            forRect.setColor(Color.WHITE);
                            canvas.drawRoundRect(info, 18, 18, forRect);
                            canvas.drawRoundRect(info, 18, 18, forRectStroke);
                            fy0TextN.setColor(Color.BLACK);
                        }
                    }
                    else {
                        RectF info = new RectF(selectedPosition+x(30), y(90), selectedPosition + x(600), y(670));
                        Paint forRect = new Paint();
                        Paint forRectStroke = new Paint();
                        forRectStroke.setStyle(Paint.Style.STROKE);
                        forRectStroke.setColor(Color.GRAY);
                        if (Main2Activity.night_mode) {
                            forRect.setColor(getResources().getColor(R.color.colorNightModeInfoTable));
                            canvas.drawRoundRect(info, 18, 18, forRect);
                            fy0TextN.setColor(Color.WHITE);
                        }
                        else {
                            forRect.setColor(Color.WHITE);
                            canvas.drawRoundRect(info, 18, 18, forRect);
                            canvas.drawRoundRect(info, 18, 18, forRectStroke);
                            fy0TextN.setColor(Color.BLACK);
                        }

                    }

                    if(selectedPosition>x(580)) {
                        canvas.drawText(sdf.format(d), selectedPosition - x(580), y(150), data);
                        canvas.drawLine(selectedPosition-x(65),y(115),selectedPosition-x(45),y(130),gray);
                        canvas.drawLine(selectedPosition-x(45),y(130),selectedPosition-x(65),y(145),gray);
                        if(charty0)canvas.drawText(String.valueOf(Math.round(Float.valueOf(Y0[arrayItemN]))*suma[arrayItemN]), selectedPosition - x(45), y(250), fy0Text);
                        if(charty1)canvas.drawText(String.valueOf(Math.round(Float.valueOf(Y1[arrayItemN]))*suma[arrayItemN]), selectedPosition - x(45), y(330), fy1Text);
                        if(charty2)canvas.drawText(String.valueOf(Math.round(Float.valueOf(Y2[arrayItemN]))*suma[arrayItemN]), selectedPosition - x(45), y(410), fy2Text);
                        if(charty3)canvas.drawText(String.valueOf(Math.round(Float.valueOf(Y3[arrayItemN]))*suma[arrayItemN]), selectedPosition - x(45), y(490), fy3Text);
                        if(charty4)canvas.drawText(String.valueOf(Math.round(Float.valueOf(Y4[arrayItemN]))*suma[arrayItemN]), selectedPosition - x(45), y(570), fy4Text);
                        if(charty5)canvas.drawText(String.valueOf(Math.round(Float.valueOf(Y5[arrayItemN]))*suma[arrayItemN]), selectedPosition - x(45), y(650), fy5Text);
                        if(charty0)canvas.drawText("Apples", selectedPosition - x(485), y(250), fy0TextN);
                        if(charty1)canvas.drawText("Oranges", selectedPosition - x(485), y(330), fy0TextN);
                        if(charty2)canvas.drawText("Lemons", selectedPosition - x(485), y(410), fy0TextN);
                        if(charty3)canvas.drawText("Apricots", selectedPosition - x(485), y(490), fy0TextN);
                        if(charty4)canvas.drawText("Kiwi", selectedPosition - x(485), y(570), fy0TextN);
                        if(charty5)canvas.drawText("Mango", selectedPosition - x(485), y(650), fy0TextN);
                        if(charty0)canvas.drawText(Math.round(Float.valueOf(Y0[arrayItemN]))+"%", selectedPosition - x(580), y(250), data);
                        if(charty1)canvas.drawText(Math.round(Float.valueOf(Y1[arrayItemN]))+"%", selectedPosition - x(580), y(330), data);
                        if(charty2)canvas.drawText(Math.round(Float.valueOf(Y2[arrayItemN]))+"%", selectedPosition - x(580), y(410), data);
                        if(charty3)canvas.drawText(Math.round(Float.valueOf(Y3[arrayItemN]))+"%", selectedPosition - x(580), y(490), data);
                        if(charty4)canvas.drawText(Math.round(Float.valueOf(Y4[arrayItemN]))+"%", selectedPosition - x(580), y(570), data);
                        if(charty5)canvas.drawText(Math.round(Float.valueOf(Y5[arrayItemN]))+"%", selectedPosition - x(580), y(650), data);

                    }
                    else{
                        canvas.drawText(sdf.format(d), selectedPosition + x(70), y(150), data);
                        canvas.drawLine(selectedPosition+x(560),y(115),selectedPosition+x(580),y(130),gray);
                        canvas.drawLine(selectedPosition+x(580),y(130),selectedPosition+x(560),y(145),gray);
                        if(charty0)canvas.drawText(String.valueOf(Math.round(Float.valueOf(Y0[arrayItemN]))*suma[arrayItemN]), selectedPosition + x(580), y(250), fy0Text);
                        if(charty1)canvas.drawText(String.valueOf(Math.round(Float.valueOf(Y1[arrayItemN]))*suma[arrayItemN]), selectedPosition + x(580), y(330), fy1Text);
                        if(charty2)canvas.drawText(String.valueOf(Math.round(Float.valueOf(Y2[arrayItemN]))*suma[arrayItemN]), selectedPosition + x(580), y(410), fy2Text);
                        if(charty3)canvas.drawText(String.valueOf(Math.round(Float.valueOf(Y3[arrayItemN]))*suma[arrayItemN]), selectedPosition + x(580), y(490), fy3Text);
                        if(charty4)canvas.drawText(String.valueOf(Math.round(Float.valueOf(Y4[arrayItemN]))*suma[arrayItemN]), selectedPosition + x(580), y(570), fy4Text);
                        if(charty5)canvas.drawText(String.valueOf(Math.round(Float.valueOf(Y5[arrayItemN]))*suma[arrayItemN]), selectedPosition + x(580), y(650), fy5Text);
                        if(charty0)canvas.drawText("Joined", selectedPosition + x(155), y(250), fy0TextN);
                        if(charty1)canvas.drawText("Left", selectedPosition + x(155), y(330), fy0TextN);
                        if(charty2)canvas.drawText("Lemons", selectedPosition + x(155), y(410), fy0TextN);
                        if(charty3)canvas.drawText("Apricots", selectedPosition + x(155), y(490), fy0TextN);
                        if(charty4)canvas.drawText("Kiwi", selectedPosition + x(155), y(570), fy0TextN);
                        if(charty5)canvas.drawText("Mango", selectedPosition + x(155), y(650), fy0TextN);
                        if(charty0)canvas.drawText(Math.round(Float.valueOf(Y0[arrayItemN]))+"%", selectedPosition + x(60), y(250), data);
                        if(charty1)canvas.drawText(Math.round(Float.valueOf(Y1[arrayItemN]))+"%", selectedPosition + x(60), y(330), data);
                        if(charty2)canvas.drawText(Math.round(Float.valueOf(Y2[arrayItemN]))+"%", selectedPosition + x(60), y(410), data);
                        if(charty3)canvas.drawText(Math.round(Float.valueOf(Y3[arrayItemN]))+"%", selectedPosition + x(60), y(490), data);
                        if(charty4)canvas.drawText(Math.round(Float.valueOf(Y4[arrayItemN]))+"%", selectedPosition + x(60), y(570), data);
                        if(charty5)canvas.drawText(Math.round(Float.valueOf(Y5[arrayItemN]))+"%", selectedPosition + x(60), y(650), data);

                    }
                }

            }//draws values in current position


        }

    private String[] arrayToZero(String[] y0) {
        String[] arr = new String[y0.length];
        for(int i = 0;i<y0.length;i++){
            arr[i]="0";
    }
        return arr;
    }


        public float Scale(float x){
    float f =x;
    if(getResources().getDisplayMetrics().densityDpi==640) {
        f = (x*0.75f);
    }
    if(getResources().getDisplayMetrics().densityDpi==480) {
        f = x;
    }
    if(getResources().getDisplayMetrics().densityDpi==320) {
        f = (x*1.5f);
    }
    if(getResources().getDisplayMetrics().densityDpi==240) {
        f = (x*2f);
    }
    if(getResources().getDisplayMetrics().densityDpi==160) {
        f = (x*3f);
    }
    if(getResources().getDisplayMetrics().densityDpi==120) {
        f = (x*4f);
    }
    return (f);
}
        public int y(int y){
        float f = y;
            if(getResources().getDisplayMetrics().densityDpi==640) {
                f = (y*1.33f);
            }
            if(getResources().getDisplayMetrics().densityDpi==480) {
                f = y;
            }
            if(getResources().getDisplayMetrics().densityDpi==320) {
                f = y*0.667f;
            }
            if(getResources().getDisplayMetrics().densityDpi==240) {
                f = (y/2);
            }
            if(getResources().getDisplayMetrics().densityDpi==160) {
                f = (y/3);
            }
            if(getResources().getDisplayMetrics().densityDpi==120) {
                f = (y*0.25f);
            }
            return (int)f;
        }
        public int x(int x) {
            float f = x;
            if(getResources().getDisplayMetrics().densityDpi==640) {
                f = (x*1.33f);
            }
            if(getResources().getDisplayMetrics().densityDpi==480) {
                f = x;
            }
            if(getResources().getDisplayMetrics().densityDpi==320) {
                f = x*0.667f;
            }
            if(getResources().getDisplayMetrics().densityDpi==240) {
                f = (x/2);
            }
            if(getResources().getDisplayMetrics().densityDpi==160) {
                f = (x/3);
            }
            if(getResources().getDisplayMetrics().densityDpi==120) {
                f = (x*0.25f);
            }
            return (int)f;

        }
        public float y(float y){
        float f = y;
        if(getResources().getDisplayMetrics().densityDpi==640) {
            f = (y*1.33f);
        }
        if(getResources().getDisplayMetrics().densityDpi==480) {
            f = y;
        }
        if(getResources().getDisplayMetrics().densityDpi==320) {
            f = y*0.667f;
        }
        if(getResources().getDisplayMetrics().densityDpi==240) {
            f = (y/2);
        }
        if(getResources().getDisplayMetrics().densityDpi==160) {
            f = (y/3);
        }
        if(getResources().getDisplayMetrics().densityDpi==120) {
            f = (y*0.25f);
        }
        return (int)f;
        }
        public float x(float x){
        float f = x;
        if(getResources().getDisplayMetrics().densityDpi==640) {
            f = (x*1.33f);
        }
        if(getResources().getDisplayMetrics().densityDpi==480) {
            f = x;
        }
        if(getResources().getDisplayMetrics().densityDpi==320) {
            f = x*0.667f;
        }
        if(getResources().getDisplayMetrics().densityDpi==240) {
            f = (x/2);
        }
        if(getResources().getDisplayMetrics().densityDpi==160) {
            f = (x/3);
        }
        if(getResources().getDisplayMetrics().densityDpi==120) {
            f = (x*0.25f);
        }
        return f;
        }
        public long Max(String[] s1,String[] s2,String[] s3,String[] s4,String[] s5,String[] s6,String[] s7){
        long max = 1;
        for(int i =0; i<s1.length;i++) {
            if (max < Long.valueOf(s1[i])) {
                max = Long.valueOf(s1[i]);
            }
        }
        for(int i =0; i<s2.length;i++) {
            if (max < Long.valueOf(s2[i])) {
                max = Long.valueOf(s2[i]);
            }
        }
        for(int i =0; i<s3.length;i++) {
            if (max < Long.valueOf(s3[i])) {
                max = Long.valueOf(s3[i]);
            }
        }
        for(int i =0; i<s4.length;i++) {
            if (max < Long.valueOf(s4[i])) {
                max = Long.valueOf(s4[i]);
            }
        }
            for(int i =0; i<s5.length;i++) {
                if (max < Long.valueOf(s5[i])) {
                    max = Long.valueOf(s5[i]);
                }
            }
            for(int i =0; i<s6.length;i++) {
                if (max < Long.valueOf(s6[i])) {
                    max = Long.valueOf(s6[i]);
                }
            }
            for(int i =0; i<s7.length;i++) {
                if (max < Long.valueOf(s7[i])) {
                    max = Long.valueOf(s7[i]);
                }
            }
        return max;
    }
        public long Max(String[] s1,String[] s2,String[] s3,String[] s4,String[] s5,String[] s6){
        long max = 1;
        for(int i =0; i<s1.length;i++) {
            if (max < Long.valueOf(s1[i])) {
                max = Long.valueOf(s1[i]);
            }
        }
        for(int i =0; i<s2.length;i++) {
            if (max < Long.valueOf(s2[i])) {
                max = Long.valueOf(s2[i]);
            }
        }
        for(int i =0; i<s3.length;i++) {
            if (max < Long.valueOf(s3[i])) {
                max = Long.valueOf(s3[i]);
            }
        }
        for(int i =0; i<s4.length;i++) {
                if (max < Long.valueOf(s4[i])) {
                    max = Long.valueOf(s4[i]);
                }
            }
            for(int i =0; i<s5.length;i++) {
                if (max < Long.valueOf(s5[i])) {
                    max = Long.valueOf(s5[i]);
                }
            }
            for(int i =0; i<s6.length;i++) {
                if (max < Long.valueOf(s6[i])) {
                    max = Long.valueOf(s6[i]);
                }
            }
        return max;
    }
        public long Max(String[] s1,String[] s2,String[] s3,String[] s4){
            long max = 1;
            for(int i =0; i<s1.length;i++) {
                if (max < Long.valueOf(s1[i])) {
                    max = Long.valueOf(s1[i]);
                }
            }
            for(int i =0; i<s2.length;i++) {
                if (max < Long.valueOf(s2[i])) {
                    max = Long.valueOf(s2[i]);
                }
            }
            for(int i =0; i<s3.length;i++) {
                if (max < Long.valueOf(s3[i])) {
                    max = Long.valueOf(s3[i]);
                }
            }
            for(int i =0; i<s4.length;i++) {
                if (max < Long.valueOf(s4[i])) {
                    max = Long.valueOf(s4[i]);
                }
            }
            return max;
        }
        public long Max(String[] s1,String[] s2,String[] s3){
            long max = 1;
            for(int i =0; i<s1.length;i++) {
                if (max < Long.valueOf(s1[i])) {
                    max = Long.valueOf(s1[i]);
                }
            }
            for(int i =0; i<s2.length;i++) {
                if (max < Long.valueOf(s2[i])) {
                    max = Long.valueOf(s2[i]);
                }
            }
            for(int i =0; i<s3.length;i++) {
                if (max < Long.valueOf(s3[i])) {
                    max = Long.valueOf(s3[i]);
                }
            }
            return max;
        }
        public long Max(String[] s1,String[] s2){
            long max = 1;
            for(int i =0; i<s1.length;i++) {
                if (max < Long.valueOf(s1[i])) {
                    max = Long.valueOf(s1[i]);
                }
            }
            for(int i =0; i<s2.length;i++) {
                if (max < Long.valueOf(s2[i])) {
                    max = Long.valueOf(s2[i]);
                }
            }
            return max;
        }
        public long Max(String[] s1){
            long max = 1;
            for(int i =0; i<s1.length;i++) {
                if (max < Long.valueOf(s1[i])) {
                    max = Long.valueOf(s1[i]);
                }
            }
            return max;
        }

        public long Min(String[] s1,String[] s2,String[] s3,String[] s4,String[] s5,String[] s6,String[] s7){
        long min = 1;
        for(int i =0; i<s1.length;i++) {
            if(i==0)min = Long.valueOf(s1[1]);
            if (min > Long.valueOf(s1[i])) {
                min = Long.valueOf(s1[i]);
            }
        }
        for(int i =0; i<s2.length;i++) {
            if (min > Long.valueOf(s2[i])) {
                min = Long.valueOf(s2[i]);
            }
        }
        for(int i =0; i<s3.length;i++) {
            if (min > Long.valueOf(s3[i])) {
                min = Long.valueOf(s3[i]);
            }
        }
        for(int i =0; i<s4.length;i++) {
            if (min > Long.valueOf(s4[i])) {
                min = Long.valueOf(s4[i]);
            }
        }
            for(int i =0; i<s5.length;i++) {
                if (min > Long.valueOf(s4[i])) {
                    min = Long.valueOf(s4[i]);
                }
            }
            for(int i =0; i<s6.length;i++) {
                if (min > Long.valueOf(s4[i])) {
                    min = Long.valueOf(s4[i]);
                }
            }
            for(int i =0; i<s7.length;i++) {
                if (min > Long.valueOf(s4[i])) {
                    min = Long.valueOf(s4[i]);
                }
            }
        return min;
    }
        public long Min(String[] s1,String[] s2,String[] s3,String[] s4,String[] s5,String[] s6){
        long min = 1;
        for(int i =0; i<s1.length;i++) {
            if(i==0)min = Long.valueOf(s1[1]);
            if (min > Long.valueOf(s1[i])) {
                min = Long.valueOf(s1[i]);
            }
        }
        for(int i =0; i<s2.length;i++) {
            if (min > Long.valueOf(s2[i])) {
                min = Long.valueOf(s2[i]);
            }
        }
        for(int i =0; i<s3.length;i++) {
            if (min > Long.valueOf(s3[i])) {
                min = Long.valueOf(s3[i]);
            }
        }
        for(int i =0; i<s4.length;i++) {
                if (min > Long.valueOf(s4[i])) {
                    min = Long.valueOf(s4[i]);
                }
            }
            for(int i =0; i<s5.length;i++) {
                if (min > Long.valueOf(s5[i])) {
                    min = Long.valueOf(s5[i]);
                }
            }
            for(int i =0; i<s6.length;i++) {
                if (min > Long.valueOf(s6[i])) {
                    min = Long.valueOf(s6[i]);
                }
            }

        return min;
    }
        public long Min(String[] s1,String[] s2,String[] s3,String[] s4){
            long min = 1;
            for(int i =0; i<s1.length;i++) {
                if(i==0)min = Long.valueOf(s1[1]);
                if (min > Long.valueOf(s1[i])) {
                    min = Long.valueOf(s1[i]);
                }
            }
            for(int i =0; i<s2.length;i++) {
                if (min > Long.valueOf(s2[i])) {
                    min = Long.valueOf(s2[i]);
                }
            }
            for(int i =0; i<s3.length;i++) {
                if (min > Long.valueOf(s3[i])) {
                    min = Long.valueOf(s3[i]);
                }
            }
            for(int i =0; i<s4.length;i++) {
                if (min > Long.valueOf(s4[i])) {
                    min = Long.valueOf(s4[i]);
                }
            }

            return min;
        }
        public long Min(String[] s1,String[] s2,String[] s3){
            long min = 1;
            for(int i =0; i<s1.length;i++) {
                if(i==0)min = Long.valueOf(s1[1]);
                if (min > Long.valueOf(s1[i])) {
                    min = Long.valueOf(s1[i]);
                }
            }
            for(int i =0; i<s2.length;i++) {
                if (min > Long.valueOf(s2[i])) {
                    min = Long.valueOf(s2[i]);
                }
            }
            for(int i =0; i<s3.length;i++) {
                if (min > Long.valueOf(s3[i])) {
                    min = Long.valueOf(s3[i]);
                }
            }

            return min;
        }
        public long Min(String[] s1,String[] s2){
            long min = 1;
            for(int i =0; i<s1.length;i++) {
                if(i==0)min = Long.valueOf(s1[1]);
                if (min > Long.valueOf(s1[i])) {
                    min = Long.valueOf(s1[i]);
                }
            }
            for(int i =0; i<s2.length;i++) {
                if (min > Long.valueOf(s2[i])) {
                    min = Long.valueOf(s2[i]);
                }
            }

            return min;
        }
        public long Min(String[] s1){
            long min = 1;
            for(int i =0; i<s1.length;i++) {
                if(i==0)min = Long.valueOf(s1[1]);
                if (min > Long.valueOf(s1[i])) {
                    min = Long.valueOf(s1[i]);
                }
            }
            return min;
        }


    }

