﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewBehaviourScript : MonoBehaviour {
    GameObject sandd1, sandd2, sandup1, sandup2,btn;


    private void Start()
    {
        sandd1 = GameObject.Find("Canvas/clock1/sanddownclock1");
        sandd2 = GameObject.Find("Canvas/clock2/sanddownclock2");
        sandup1 = GameObject.Find("Canvas/clock1/sandupclock1");
        sandup2 = GameObject.Find("Canvas/clock2/sandupclock2");
        btn = GameObject.Find("Canvas/Button");
        btn.GetComponent<Button>().onClick.AddListener(OnMDown);

    }
    private void OnMDown()
    {
        Debug.Log("sanddown1 "+sandd1.transform.localPosition);
        Debug.Log("sanddown2 " + sandd2.transform.localPosition);
        Debug.Log("sanup1 " + sandup1.transform.localPosition);
        Debug.Log("sanup2 " + sandup2.transform.localPosition);
    }
}
