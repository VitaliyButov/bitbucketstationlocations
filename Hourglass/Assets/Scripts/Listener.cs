﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Listener : MonoBehaviour {

    GameObject ButtonPlay;
	// Use this for initialization
	void Start () {
        ButtonPlay = GameObject.Find("Canvas/PlayButton");
        ButtonPlay.GetComponent<Button>().onClick.AddListener(play);
		
	}
	
    void play()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
	// Update is called once per frame
	void Update () {
		
	}
}
