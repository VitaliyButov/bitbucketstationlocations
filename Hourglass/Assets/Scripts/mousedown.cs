﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading;
using System;

public class mousedown : MonoBehaviour {

	public Sprite pesochniyeChasiIdle;
	private GameObject  clock1, 
						clock2,
						sanddownclock1,
						sandupclock1,
						sanddownclock2,
						sandupclock2,
						minutes1, 
						minutes2,
						reverseButton,
						continueButton,
						canvas;

    private float clock1timeOriginal,
                clock2timeOriginal,lastPositionDown1, lastPositionUp1, lastPositionDown2, lastPositionUp2,
        lastPositionDownAfterSet1, lastPositionUpAfterSet1, lastPositionDownAfterSet2, lastPositionUpAfterSet2, float1, float2;
				
	public GameObject winText;
    private Coroutine sandDrop1, sandDrop2,stoper;

	void Start(){
                
        
		clock1timeOriginal = GameManager.clock1timeorg;
		clock2timeOriginal = GameManager.clock2timeorg;

        float1 = 1;
        float2 = 1;

        canvas = GameObject.Find ("Canvas");

		clock1 = GameObject.Find("/Canvas/clock1");
		clock2 = GameObject.Find("/Canvas/clock2");
		minutes1 = GameObject.Find ("/Canvas/podstavka1/minutes1");
		minutes2 = GameObject.Find ("/Canvas/podstavka2/minutes2");
		sanddownclock1 = GameObject.Find ("Canvas/clock1/sanddownclock1");
		sandupclock1 = GameObject.Find ("Canvas/clock1/sandupclock1");
		sanddownclock2 = GameObject.Find ("Canvas/clock2/sanddownclock2");
		sandupclock2 = GameObject.Find ("Canvas/clock2/sandupclock2");

        lastPositionDown1 = sanddownclock1.transform.position.y;
        lastPositionDown2 = sanddownclock2.transform.position.y;
        lastPositionUp1 = sandupclock1.transform.position.y;
        lastPositionUp2 = sandupclock2.transform.position.y;
        /*sanddownclock1.transform.localPosition = new Vector3(-(Screen.width* 0.00075416f),-Screen.height* 0.0025f,20);
        sanddownclock2.transform.localPosition = new Vector3(Screen.width * 0.0007395f, -Screen.height * 0.0025f, 20);
            sandupclock1.transform.localPosition=new Vector3(-(Screen.width * 0.00075416f),Screen.height* 0.0024f, 20);
            sandupclock2.transform.localPosition=new Vector3(Screen.width * 0.0007395f, Screen.height * 0.0024f, 20);*/




        reverseButton = GameObject.Find("/Canvas/ReverseButton");
		continueButton = GameObject.Find("/Canvas/ContinueButton");


		reverseButton.GetComponent<Button> ().onClick.AddListener (OnClick);

       




	}

   	public void OnClick(){
        StartCoroutine(Compare());
    }

    IEnumerator Compare()
    {
        if (GameManager.selected1.activeSelf && GameManager.selected2.activeSelf)
        {
            GameManager.selected1.SetActive(false);
            GameManager.selected2.SetActive(false);
            StartCoroutine(Reverse1());
            yield return StartCoroutine(Reverse2());
        }

        if (GameManager.selected1.activeSelf && !GameManager.selected2.activeSelf)
        {
            GameManager.selected1.SetActive(false);
            yield return StartCoroutine(Reverse1());
        }

        if (!GameManager.selected1.activeSelf && GameManager.selected2.activeSelf)
        {
            GameManager.selected2.SetActive(false);
            yield return StartCoroutine(Reverse2());
        }
        StartCoroutine(OnClickContinue());
        

    }

    IEnumerator Reverse1()
    {
        for (float a = 0; a <= 180; a = a + 5)
        {
            clock1.transform.localEulerAngles = new Vector3(0, 0, a);

            yield return new WaitForSeconds(0.01f);
        }
        clock1.transform.localRotation = new Quaternion(0, 0, 0, 0);
        sandupclock1.transform.localPosition = new Vector3(sandupclock1.transform.localPosition.x, 3.3f, 0);
        sanddownclock1.transform.localPosition = new Vector3(sanddownclock1.transform.localPosition.x, -12,0);
        GameManager.reversed1 = true;

    }

    IEnumerator Reverse2()
    {
        for (float a = 0; a <= 180; a = a + 5)
        {
            clock2.transform.localEulerAngles = new Vector3(0, 0, a);
            yield return new WaitForSeconds(0.01f);
        }
        clock2.transform.localRotation = new Quaternion(0, 0, 0, 0);
        sandupclock2.transform.localPosition = new Vector3(sandupclock2.transform.localPosition.x, 3.3f, 0);
        sanddownclock2.transform.localPosition = new Vector3(sanddownclock2.transform.localPosition.x, -12, 0);
        GameManager.reversed2 = true;
        
        
    }

    IEnumerator OnClickContinue(){
		if (GameManager.reversed2 && GameManager.reversed1) 
        {
                    GameManager.reversed1 = false;
                    GameManager.reversed2 = false;
            if (GameManager.clock2time > GameManager.clock1time) {
                StartCoroutine (Giveresult(1,2));						
			}
			if (GameManager.clock1time > GameManager.clock2time) {
                StartCoroutine (Giveresult(1,1));				
			}
        }//оба перевёрнуты
        if (!GameManager.reversed2 && GameManager.reversed1 && sandDrop2 != null)
        {
            GameManager.reversed1 = false;
            if (GameManager.clock2time > GameManager.clock1time)
            {
                StartCoroutine(Giveresult(2,2));
            }
            if (GameManager.clock1time > GameManager.clock2time)
            {
                StartCoroutine(Giveresult(2,1));
            }
        }//первые перевёрнуты вторые всё ещё сыпятся
        if (!GameManager.reversed2 && GameManager.reversed1 && sandDrop2 ==null)
        {           
            GameManager.reversed1 = false;
            yield return sandDrop1 = StartCoroutine(SandDrop (1,sanddownclock1.transform.localPosition.y,sandupclock1.transform.localPosition.y));
			if(GameManager.clock1time == GameManager.clock1timeorg)StartCoroutine(SetDefault(1));
        }//первые часы перевёрнуты а вторые нет
        if (GameManager.reversed2 && !GameManager.reversed1 && sandDrop1 != null)
        {
            GameManager.reversed2 = false;
            if (GameManager.clock2time > GameManager.clock1time)
            {
                StartCoroutine(Giveresult(3,2));
            }
            if (GameManager.clock1time > GameManager.clock2time)
            {
                StartCoroutine(Giveresult(3,1));
            }
        }//вторые перевёрнуты первые всё ещё сыпятся
        if (GameManager.reversed2 && !GameManager.reversed1 && sandDrop1 ==null)
        {            
            GameManager.reversed2 = false;
            yield return sandDrop2 = StartCoroutine(SandDrop(2, sanddownclock2.transform.localPosition.y, sandupclock2.transform.localPosition.y));
            if (GameManager.clock2time == GameManager.clock2timeorg) StartCoroutine(SetDefault(2));
        }//вторые часы перевёрнуты а первые нет
    }

    IEnumerator Giveresult(int variant,int i)
    {

        if (stoper == null) stoper = StartCoroutine(ostanavlivatel());
        yield return null;
        if(variant == 1) {
            if (i == 1)
            {
                GameManager.clock1time = GameManager.clock1time - GameManager.clock2time;
                sandDrop1 = StartCoroutine(SandDrop(1, sanddownclock1.transform.localPosition.y, sandupclock1.transform.localPosition.y));
                sandDrop2 = StartCoroutine(SandDrop(2, sanddownclock2.transform.localPosition.y, sandupclock2.transform.localPosition.y));
            }

            if (i == 2)
            {
                GameManager.clock2time = GameManager.clock2time - GameManager.clock1time;
                sandDrop1 = StartCoroutine(SandDrop(1, sanddownclock1.transform.localPosition.y, sandupclock1.transform.localPosition.y));
                sandDrop2 = StartCoroutine(SandDrop(2, sanddownclock2.transform.localPosition.y, sandupclock2.transform.localPosition.y));
            }
        }
        if(variant == 2)
        {
            if (i == 1)
            {
                GameManager.clock1time = GameManager.clock1time - GameManager.clock2time;
                yield return sandDrop1 = StartCoroutine(SandDrop(1, sanddownclock1.transform.localPosition.y, sandupclock1.transform.localPosition.y));

            }

            if (i == 2)
            {
                GameManager.clock2time = GameManager.clock2time - GameManager.clock1time;
                yield return sandDrop1 = StartCoroutine(SandDrop(1, sanddownclock1.transform.localPosition.y, sandupclock1.transform.localPosition.y));


            }
        }
        if (variant == 3)
        {
            if (i == 1)
            {
                GameManager.clock1time = GameManager.clock1time - GameManager.clock2time;                
                sanddownclock1.transform.localPosition = new Vector2(sanddownclock1.transform.localPosition.x, lastPositionDown1);
                sandupclock1.transform.localPosition = new Vector2(sandupclock1.transform.localPosition.x, lastPositionUp1);
                yield return sandDrop2 = StartCoroutine(SandDrop(2, sanddownclock2.transform.localPosition.y, sandupclock2.transform.localPosition.y));

            }

            if (i == 2)
            {
                GameManager.clock2time = GameManager.clock2time - GameManager.clock1time;
                sanddownclock1.transform.localPosition = new Vector2(sanddownclock1.transform.localPosition.x, lastPositionDown1);
                sandupclock1.transform.localPosition = new Vector2(sandupclock1.transform.localPosition.x, lastPositionUp1);
                yield return sandDrop2 = StartCoroutine(SandDrop(2, sanddownclock2.transform.localPosition.y, sandupclock2.transform.localPosition.y));
            }
        }
        
    }

    IEnumerator SandDrop(int ii,float posDown,float posUp)
	{
		if(ii ==1){
		GameManager.particle1.SetActive (true);
            sanddownclock1.transform.localPosition = new Vector2(sanddownclock1.transform.localPosition.x,posDown);
            sandupclock1.transform.localPosition = new Vector2(sandupclock1.transform.localPosition.x,posUp);
		for(float i = float1; i<=55*GameManager.clock1timeorg;i++){
                
			sanddownclock1.transform.localPosition = new Vector3 (sanddownclock1.transform.localPosition.x,sanddownclock1.transform.localPosition.y+0.12f /GameManager.clock1timeorg, 0);
			sandupclock1.transform.localPosition = new Vector3 (sandupclock1.transform.localPosition.x,sandupclock1.transform.localPosition.y - 0.12f / GameManager.clock1timeorg,0);
            float1 = (float)i;
            yield return new WaitForSeconds(0.03f);
		}
		GameManager.particle1.SetActive (false);
		//yield return new WaitForSeconds (0.1f);

		
			yield return StartCoroutine(SetDefault (1));
            GameManager.reversed1 = false;

            


        }
		if(ii == 2){
			GameManager.particle2.SetActive (true);
            sanddownclock2.transform.localPosition = new Vector2(sanddownclock2.transform.localPosition.x, posDown);
            sandupclock2.transform.localPosition = new Vector2(sandupclock2.transform.localPosition.x, posUp);
            for (float i = float2;i<=55*GameManager.clock2timeorg;i++){
                
                sanddownclock2.transform.localPosition = new Vector3 (sanddownclock2.transform.localPosition.x, sanddownclock2.transform.localPosition.y+(0.12f / GameManager.clock2timeorg), 0);
				sandupclock2.transform.localPosition = new Vector3 (sandupclock2.transform.localPosition.x,sandupclock2.transform.localPosition.y-(0.12f / GameManager.clock2timeorg),0);
                float2 = (float)i;
                yield return new WaitForSeconds(0.03f);
			}
			GameManager.particle2.SetActive (false);
			//yield return new WaitForSeconds (0.1f);

                yield return StartCoroutine(SetDefault(2));
				GameManager.reversed2 = false;
		}

	}

    IEnumerator SetDefault(int i){
        yield return null;
		if (i == 1) {
			sanddownclock1.transform.localPosition = new Vector3 (sanddownclock1.transform.localPosition.x, sanddownclock1.transform.localPosition.y, 0);
			sandupclock1.transform.localPosition = new Vector3 (sandupclock1.transform.localPosition.x, 11.3f, 0);
			GameManager.clock1time = GameManager.clock1timeorg;
            //minutes1.GetComponent<Text> ().text = clock1timeOriginal + " мин";
            lastPositionDown1 = sanddownclock1.transform.localPosition.y;
            lastPositionUp1 = sandupclock1.transform.localPosition.y;
            lastPositionDown2 = sanddownclock2.transform.localPosition.y;
            lastPositionUp2 = sandupclock2.transform.localPosition.y;
            float1 = 1;

        }
		if (i == 2) {
			sanddownclock2.transform.localPosition = new Vector3 (sanddownclock2.transform.localPosition.x, sanddownclock2.transform.localPosition.y, 0);
			sandupclock2.transform.localPosition = new Vector3 (sandupclock2.transform.localPosition.x, 11.3f, 0);
			GameManager.clock2time = GameManager.clock2timeorg;
            //minutes2.GetComponent<Text> ().text = clock2timeOriginal + " мин";
            lastPositionDown1 = sanddownclock1.transform.localPosition.y;
            lastPositionUp1 = sandupclock1.transform.localPosition.y;
            lastPositionDown2 = sanddownclock2.transform.localPosition.y;
            lastPositionUp2 = sandupclock2.transform.localPosition.y;
            float2 = 1;
        }
        CheckWin();
    }

	void CheckWin(){
		if (GameManager.clock1time == GameManager.winResult) {
			winText = Instantiate (winText,canvas.transform);
            minutes1.GetComponent<Text>().text = GameManager.clock1time + " мин";
        }
        if(GameManager.clock2time == GameManager.winResult)
        {
            winText = Instantiate(winText, canvas.transform);
            minutes2.GetComponent<Text>().text = GameManager.clock2time + " мин";
        }
    }

	IEnumerator ostanavlivatel()
    {
        while(true)
        {
            while (!GameManager.selected1.activeSelf && !GameManager.selected2.activeSelf)
            {
                yield return null;
            }
            if (GameManager.selected1.activeSelf || GameManager.selected2.activeSelf)
            {
                if (sandDrop1 != null) StopCoroutine(sandDrop1);
                if (sandDrop2 != null) StopCoroutine(sandDrop2);
                lastPositionDown1 = sanddownclock1.transform.localPosition.y;
                lastPositionUp1 = sandupclock1.transform.localPosition.y;
                lastPositionDown2 = sanddownclock2.transform.localPosition.y;
                lastPositionUp2 = sandupclock2.transform.localPosition.y;
                while (GameManager.selected1.activeSelf || GameManager.selected2.activeSelf)
                {
                    yield return null;
                }
                if (GameManager.particle1.activeSelf)
                {
                    sandDrop1 = StartCoroutine(SandDrop(1, lastPositionDown1, lastPositionUp1));
                }
                if (GameManager.particle2.activeSelf)
                {
                    sandDrop2 = StartCoroutine(SandDrop(2, lastPositionDown2, lastPositionUp2));
                }

            }
        }
        
    }



}

