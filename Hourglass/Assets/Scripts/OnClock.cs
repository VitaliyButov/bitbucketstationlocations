﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnClock : MonoBehaviour {




    GameObject clock1button, clock2button;
    private void Start()
    {
        clock1button = GameObject.Find("Canvas/clock1/clock1button");
        clock2button = GameObject.Find("Canvas/clock2/clock2button");
        clock1button.GetComponent<Button>().onClick.AddListener(clock1Push);
        clock2button.GetComponent<Button>().onClick.AddListener(clock2Push);
    }

    private void clock1Push()
    {
        if (!GameManager.selected1.activeSelf)
        {
            GameManager.selected1.SetActive(true);
        }
        else
        {

            GameManager.selected1.SetActive(false);
        }
    }
    private void clock2Push()
    {
        if (!GameManager.selected2.activeSelf)
        {

            GameManager.selected2.SetActive(true);
        }
        else
        {

            GameManager.selected2.SetActive(false);
        }
    }

    /*
      Input.mousePosition.x
      GameObject clock1 = Instantiate(clock);
    clock1.transform.localPosition = new Vector3(-clock1.transform.localPosition.x, 0, 20);
    clock1.transform.SetParent(canvas.transform);
        clock1.transform.localScale = new Vector3(32.5f, 32.5f, 0);
    clock1.transform.localRotation = new Quaternion(0, 0, 0, 0);
    clock1.name = "clock1";
        GameObject clock2 = Instantiate(clock);
    clock2.transform.localPosition = new Vector3(clock2.transform.localPosition.x, 0, 20);
    clock2.transform.SetParent(canvas.transform);
		clock2.transform.localScale = new Vector3(32.5f,32.5f,0);
    clock2.transform.localRotation = new Quaternion(0,0,0,0);
    clock2.name = "clock2";
		

		GameObject podstavka1 = Instantiate(podstavka);
    podstavka1.transform.SetParent(canvas.transform);
        podstavka1.transform.localPosition = new Vector3(clock1.transform.localPosition.x, clock1.transform.localPosition.y-380f,0.0f);
    podstavka1.transform.localScale = new Vector3(39.8f,34.2f,0.0f);

    GameObject text1 = Instantiate(minutes);
    text1.transform.SetParent(canvas.transform);
		text1.name = "minutes1";
        text1.transform.localPosition = new Vector3(clock1.transform.localPosition.x, clock1.transform.localPosition.y-350f,0.0f);
    text1.transform.localScale = new Vector3(1.0f,1.0f,0.0f);
    text1.transform.localRotation = new Quaternion(0, 0, 0, 0);
    text1.transform.GetComponent<Text>().text = clock1time + " мин";


		GameObject podstavka2 = Instantiate(podstavka);
    podstavka2.transform.SetParent(canvas.transform);
        podstavka2.transform.localPosition = new Vector3(clock2.transform.localPosition.x, clock2.transform.localPosition.y-380f,0.0f);
    podstavka2.transform.localScale = new Vector3(39.8f,34.2f,0.0f);


    GameObject text2 = Instantiate(minutes);
    text2.transform.SetParent(canvas.transform);
		text2.name = "minutes2";
        text2.transform.localPosition = new Vector3(clock2.transform.localPosition.x, clock2.transform.localPosition.y-350f,0.0f);
    text2.transform.localScale = new Vector3(1.0f,1.0f,0.0f);
    text2.transform.localRotation = new Quaternion(0, 0, 0, 0);
    text2.transform.GetComponent<Text>().text = clock2time + " мин";

		selected1 = new GameObject("clock1selected",typeof(SpriteRenderer));
    selected2 = new GameObject("clock2selected",typeof(SpriteRenderer));
    selected1.GetComponent<SpriteRenderer>().sprite = sprite;
		selected2.GetComponent<SpriteRenderer>().sprite = sprite;
		selected1.transform.localPosition = new Vector3(-2.5f, 0.0f, 20.0f);
    selected2.transform.localPosition = new Vector3(2.5f, 0.0f, 20.0f);
    selected1.transform.SetParent(canvas.transform);
		selected2.transform.SetParent(canvas.transform);
		selected1.transform.localScale = clock1.transform.localScale;
		selected2.transform.localScale = clock2.transform.localScale;
		selected1.GetComponent<SpriteRenderer>().sortingOrder = 4;
		selected2.GetComponent<SpriteRenderer>().sortingOrder = 4;
		selected1.SetActive(false);
		selected2.SetActive(false);


		reverseButton = Instantiate(reverseButton, canvas.transform);
    reverseButton.name = "ReverseButton";
		reverseButton.transform.localRotation = new Quaternion(0, 0, 0, 0);
    reverseButton.transform.localPosition = new Vector3(-780, 450, 0.0f);

    continueButton = Instantiate(continueButton, canvas.transform);
    continueButton.name = "ContinueButton";
		continueButton.transform.localRotation = new Quaternion(0, 0, 0, 0);
    continueButton.transform.localPosition = new Vector3(-780, 380, 0.0f);*/
}
