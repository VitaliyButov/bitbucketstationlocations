package numbernolodin.com.charts;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.hardware.input.InputManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.CompoundButtonCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import numbernolodin.com.charts.view.CustomView;


    public class Main2Activity extends AppCompatActivity {
    CustomView cs;
    Button endFirstEdge,startSecondEdge,middle,graphic;
    boolean endFirstEdgeBoolean,startSecondEdgeBoolean;
    private Toolbar toolbar2;
    float f,f1,button1minValue,button2maxValue;
    RelativeLayout lr,main;

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
                getMenuInflater().inflate(R.menu.night_mode, menu);
                return true;
        }



        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        cs = (CustomView) findViewById(R.id.customView);
        lr = (RelativeLayout) findViewById(R.id.LaineLayout);
        main = (RelativeLayout) findViewById(R.id.Mainl);

        toolbar2 = findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar2);

        cs.selectedPosition = 0;

        endFirstEdge = new Button(this);
        startSecondEdge = new Button(this);
        middle = new Button(this);
        graphic = new Button(this);

        //endFirstEdge.setBackgroundColor(getResources().getColor(R.color.transparent));
        //startSecondEdge.setBackgroundColor(getResources().getColor(R.color.transparent));
        //middle.setBackgroundColor(getResources().getColor(R.color.transparent));
        graphic.setBackgroundColor(getResources().getColor(R.color.transparent));
        endFirstEdge.setTranslationY(cs.y(1115)+cs.y(168));
        endFirstEdge.setTranslationX(cs.x(-35));
        startSecondEdge.setTranslationY(cs.y(1115)+cs.y(168));
        startSecondEdge.setTranslationX(cs.x(865));
        middle.setTranslationY(cs.y(1115)+cs.y(168));
        middle.setTranslationX(cs.x(415));
        graphic.setTranslationX(cs.x(415));
        graphic.setTranslationY(cs.y(620));


        cs.chartf0 = true;
        cs.chartf1 = true;
        cs.chartf2 = true;
        cs.chartf3 = true;
        cs.charty0=true;
        cs.charty1=true;

        endFirstEdge.setScaleX(0.3f);
        endFirstEdge.setScaleY(1.2f);
        startSecondEdge.setScaleX(0.3f);
        startSecondEdge.setScaleY(1.2f);
        middle.setScaleX(3.3f);
        middle.setScaleY(1.2f);
        graphic.setScaleX(3.4f);
        graphic.setScaleY(7f);
        cs.endRect = cs.x(100);
        cs.startRect = cs.x(1000);
        lr.addView(graphic);
        lr.addView(middle);
        lr.addView(endFirstEdge);
        lr.addView(startSecondEdge);

        button1minValue = endFirstEdge.getX();
        button2maxValue = startSecondEdge.getX();

            middle.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN :f = event.getRawX();
                        case MotionEvent.ACTION_MOVE :
                            if((event.getRawX()-f)<0 && endFirstEdge.getTranslationX()!=button1minValue) {
                                startSecondEdge.setTranslationX(clamp((startSecondEdge.getTranslationX()+(event.getRawX()-f)),endFirstEdge.getTranslationX()+cs.x(80),button2maxValue));
                                cs.startRect = clamp((cs.startRect+(event.getRawX()-f)),cs.endRect+cs.x(80),cs.x(1000));
                                endFirstEdge.setTranslationX(clamp((endFirstEdge.getTranslationX()+(event.getRawX()-f)),button1minValue,startSecondEdge.getX()-cs.x(80)));
                                cs.endRect = clamp((cs.endRect+(event.getRawX()-f)),cs.x(100),cs.startRect-cs.x(80));
                                middle.setTranslationX(middle.getTranslationX() + (event.getRawX() - f));
                            }
                            if((event.getRawX()-f)>0 && startSecondEdge.getTranslationX()!=button2maxValue) {
                                startSecondEdge.setTranslationX(clamp((startSecondEdge.getTranslationX()+(event.getRawX()-f)),endFirstEdge.getTranslationX()+cs.x(80),button2maxValue));
                                cs.startRect = clamp((cs.startRect+(event.getRawX()-f)),cs.endRect+cs.x(80),cs.x(1000));
                                endFirstEdge.setTranslationX(clamp((endFirstEdge.getTranslationX()+(event.getRawX()-f)),button1minValue,startSecondEdge.getX()-cs.x(80)));
                                cs.endRect = clamp((cs.endRect+(event.getRawX()-f)),cs.x(100),cs.startRect-cs.x(80));
                                middle.setTranslationX(middle.getTranslationX() + (event.getRawX() - f));
                            }
                            cs.postInvalidate();
                            f=event.getRawX();


                    }
                    return false;
                }
            });

        endFirstEdge.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN :f = event.getRawX();
                    case MotionEvent.ACTION_MOVE :
                        endFirstEdge.setTranslationX(clamp((endFirstEdge.getTranslationX()+(event.getRawX()-f)),button1minValue,startSecondEdge.getX()-cs.x(80)));
                        cs.endRect = clamp((cs.endRect+(event.getRawX()-f)),cs.x(100),cs.startRect-cs.x(80));
                        if((event.getRawX()-f)>0 && cs.endRect!=cs.startRect-cs.x(80)) {
                            middle.setTranslationX(middle.getTranslationX() + (event.getRawX() - f) / 2);
                            middle.setScaleX(middle.getScaleX() - (3.3f / cs.x(900) * (event.getRawX() - f)));
                        }
                        if((event.getRawX()-f)<0 && cs.endRect!=cs.x(100)) {
                            middle.setTranslationX(middle.getTranslationX() + (event.getRawX() - f) / 2);
                            middle.setScaleX(middle.getScaleX() - (3.3f / cs.x(900) * (event.getRawX() - f)));
                        }
                        cs.postInvalidate();
                        f=event.getRawX();


                }
                return false;
            }
        });
        startSecondEdge.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN :f = event.getRawX();
                    case MotionEvent.ACTION_MOVE :
                        startSecondEdge.setTranslationX(clamp((startSecondEdge.getTranslationX()+(event.getRawX()-f)),endFirstEdge.getTranslationX()+cs.x(80),button2maxValue));
                        cs.startRect = clamp((cs.startRect+(event.getRawX()-f)),cs.endRect+cs.x(80),cs.x(1000));
                        if((event.getRawX()-f)<0 && cs.startRect!=cs.endRect+cs.x(80)) {
                            middle.setTranslationX(middle.getTranslationX() + (event.getRawX() - f) / 2);
                            middle.setScaleX(middle.getScaleX() + (3.3f / cs.x(900) * (event.getRawX() - f)));
                        }
                        if((event.getRawX()-f)>0 && startSecondEdge.getTranslationX()!=button2maxValue) {
                            middle.setTranslationX(middle.getTranslationX() + (event.getRawX() - f) / 2);
                            middle.setScaleX(middle.getScaleX() + (3.3f / cs.x(900) * (event.getRawX() - f)));
                        }
                        cs.postInvalidate();
                        f=event.getRawX();

                }
                return false;
            }
        });

        graphic.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        float arrayItemNCoeff = (event.getRawX() - cs.x(100)) / cs.x(900);
                        int arrayItemN = Math.round(cs.X.length * arrayItemNCoeff);
                        cs.selectedPosition = (int) (((Long.valueOf(cs.X[arrayItemN]) - cs.datemin) * cs.x(900) / cs.date) + cs.x(100));
                        cs.postInvalidate();

                    case MotionEvent.ACTION_MOVE:
                        arrayItemNCoeff = (event.getRawX() - cs.x(100)) / cs.x(900);
                        arrayItemN = Math.round(cs.X.length * arrayItemNCoeff);
                        cs.selectedPosition = (int) (((Long.valueOf(cs.X[arrayItemN]) - cs.datemin) * cs.x(900) / cs.date) + cs.x(100));
                        cs.postInvalidate();
                }
            return false;
            }
        });


        if(cs.getChart()=="fifth"){
            ColorStateList csl1 = new ColorStateList(
                    new int[][] {
                            //new int[] { android.R.attr.state_enabled}, // enabled
                            //new int[] {-android.R.attr.state_enabled}, // disabled
                            new int[] {-android.R.attr.state_checked}, // unchecked
                            new int[] {android.R.attr.state_checked}, // checked
                            //new int[] { android.R.attr.state_pressed}  // pressed
                    },

                    new int[] {
                            //Color.BLACK,
                            //Color.RED,
                            getResources().getColor(R.color.colorFY0),
                            getResources().getColor(R.color.colorFY0),
                            //Color.BLUE
                    }

            );
            ColorStateList csl2 = new ColorStateList(
                    new int[][] {
                            //new int[] { android.R.attr.state_enabled}, // enabled
                            //new int[] {-android.R.attr.state_enabled}, // disabled
                            new int[] {-android.R.attr.state_checked}, // unchecked
                            new int[] {android.R.attr.state_checked}, // checked
                            //new int[] { android.R.attr.state_pressed}  // pressed
                    },

                    new int[] {
                            //Color.BLACK,
                            //Color.RED,
                            getResources().getColor(R.color.colorFY1),
                            getResources().getColor(R.color.colorFY1),
                            //Color.BLUE
                    }

            );ColorStateList csl3 = new ColorStateList(
                    new int[][] {
                            //new int[] { android.R.attr.state_enabled}, // enabled
                            //new int[] {-android.R.attr.state_enabled}, // disabled
                            new int[] {-android.R.attr.state_checked}, // unchecked
                            new int[] {android.R.attr.state_checked}, // checked
                            //new int[] { android.R.attr.state_pressed}  // pressed
                    },

                    new int[] {
                            //Color.BLACK,
                            //Color.RED,
                            getResources().getColor(R.color.colorFY2),
                            getResources().getColor(R.color.colorFY2),
                            //Color.BLUE
                    }

            );ColorStateList csl4 = new ColorStateList(
                    new int[][] {
                            //new int[] { android.R.attr.state_enabled}, // enabled
                            //new int[] {-android.R.attr.state_enabled}, // disabled
                            new int[] {-android.R.attr.state_checked}, // unchecked
                            new int[] {android.R.attr.state_checked}, // checked
                            //new int[] { android.R.attr.state_pressed}  // pressed
                    },

                    new int[] {
                            //Color.BLACK,
                            //Color.RED,
                            getResources().getColor(R.color.colorFY3),
                            getResources().getColor(R.color.colorFY3),
                            //Color.BLUE
                    }

            );
        CheckBox checkBox0 = new CheckBox(this);
        checkBox0.setText("#y0");
        CheckBox checkBox1 = new CheckBox(this);
        checkBox1.setText("#y1");
        CheckBox checkBox2 = new CheckBox(this);
        checkBox2.setText("#y2");
        CheckBox checkBox3 = new CheckBox(this);
        checkBox3.setText("#y3");
        checkBox0.setTranslationX(100);
        checkBox0.setTranslationY(1250+cs.y(168));
        checkBox1.setTranslationX(100);
        checkBox1.setTranslationY(1350+cs.y(168));
        checkBox2.setTranslationX(100);
        checkBox2.setTranslationY(1450+cs.y(168));
        checkBox3.setTranslationX(100);
        checkBox3.setTranslationY(1550+cs.y(168));
        checkBox0.setId(R.id.CheckboxFY0);
        checkBox1.setId(R.id.CheckboxFY1);
        checkBox2.setId(R.id.CheckboxFY2);
        checkBox3.setId(R.id.CheckboxFY3);
        CompoundButtonCompat.setButtonTintList(checkBox0, csl1);
        CompoundButtonCompat.setButtonTintList(checkBox1, csl2);
        CompoundButtonCompat.setButtonTintList(checkBox2, csl3);
        CompoundButtonCompat.setButtonTintList(checkBox3, csl4);
        checkBox0.setChecked(true);
        checkBox1.setChecked(true);
        checkBox2.setChecked(true);
        checkBox3.setChecked(true);
        lr.addView(checkBox0);
        lr.addView(checkBox1);
        lr.addView(checkBox2);
        lr.addView(checkBox3);

            checkBox0.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        cs.chartf0 = true;
                        cs.postInvalidate();
                    }
                    else{
                        cs.chartf0 = false;
                        cs.postInvalidate();
                    }
                }
            });
            checkBox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        cs.chartf1 = true;
                        cs.postInvalidate();
                    }
                    else{
                        cs.chartf1 = false;
                        cs.postInvalidate();
                    }
                }
            });
            checkBox2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        cs.chartf2 = true;
                        cs.postInvalidate();
                    }
                    else{
                        cs.chartf2 = false;
                        cs.postInvalidate();
                    }
                }
            });
            checkBox3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        cs.chartf3 = true;
                        cs.postInvalidate();
                    }
                    else{
                        cs.chartf3 = false;
                        cs.postInvalidate();
                    }
                }
            });

        }
        else {
            ColorStateList csl1 = new ColorStateList(
            new int[][] {
                    //new int[] { android.R.attr.state_enabled}, // enabled
                    //new int[] {-android.R.attr.state_enabled}, // disabled
                    new int[] {-android.R.attr.state_checked}, // unchecked
                    new int[] {android.R.attr.state_checked}, // checked
                    //new int[] { android.R.attr.state_pressed}  // pressed
            },

            new int[] {
                    //Color.BLACK,
                    //Color.RED,
                    getResources().getColor(R.color.colorY0),
                    getResources().getColor(R.color.colorY0),
                    //Color.BLUE
            }

            );
            ColorStateList csl2 = new ColorStateList(
                    new int[][] {
                            //new int[] { android.R.attr.state_enabled}, // enabled
                            //new int[] {-android.R.attr.state_enabled}, // disabled
                            new int[] {-android.R.attr.state_checked}, // unchecked
                            new int[] {android.R.attr.state_checked}, // checked
                            //new int[] { android.R.attr.state_pressed}  // pressed
                    },

                    new int[] {
                            //Color.BLACK,
                            //Color.RED,
                            getResources().getColor(R.color.colorY1),
                            getResources().getColor(R.color.colorY1),
                            //Color.BLUE
                    }

            );
            CheckBox checkBox0 = new CheckBox(this);
            CheckBox checkBox1 = new CheckBox(this);
            checkBox0.setText("#y0");
            checkBox0.setId(R.id.CheckboxY0);
            checkBox1.setText("#y1");
            checkBox1.setId(R.id.CheckboxY1);
            checkBox0.setTranslationX(100);
            checkBox0.setTranslationY(1250+cs.y(168));
            checkBox1.setTranslationX(100);
            checkBox1.setTranslationY(1350+cs.y(168));
            CompoundButtonCompat.setButtonTintList(checkBox0, csl1);
            CompoundButtonCompat.setButtonTintList(checkBox1, csl2);
            checkBox0.setChecked(true);
            checkBox1.setChecked(true);
            lr.addView(checkBox0);
            lr.addView(checkBox1);

            checkBox0.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        cs.charty0 = true;
                        cs.postInvalidate();
                    }
                    else{
                        cs.charty0 = false;
                        cs.postInvalidate();
                    }
                }
            });
            checkBox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        cs.charty1 = true;
                        cs.postInvalidate();
                    }
                    else{
                        cs.charty1 = false;
                        cs.postInvalidate();
                    }
                }
            });
        }
            if(MainActivity.night_mode){
                toolbar2.setBackgroundColor(getResources().getColor(R.color.colorNightMode));
                main.setBackgroundColor(getResources().getColor(R.color.colorNightModeLayout));
                if(cs.getChart()=="fifth") {
                    ((CheckBox) findViewById(R.id.CheckboxFY0)).setTextColor(Color.WHITE);
                    ((CheckBox) findViewById(R.id.CheckboxFY1)).setTextColor(Color.WHITE);
                    ((CheckBox) findViewById(R.id.CheckboxFY2)).setTextColor(Color.WHITE);
                    ((CheckBox) findViewById(R.id.CheckboxFY3)).setTextColor(Color.WHITE);
                }
                else{
                        ((CheckBox)findViewById(R.id.CheckboxY0)).setTextColor(Color.WHITE);
                        ((CheckBox)findViewById(R.id.CheckboxY1)).setTextColor(Color.WHITE);
                }
            }
            else{
                toolbar2.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                main.setBackgroundColor(Color.WHITE);
                if(cs.getChart()=="fifth") {
                    ((CheckBox)findViewById(R.id.CheckboxFY0)).setTextColor(Color.BLACK);
                    ((CheckBox)findViewById(R.id.CheckboxFY1)).setTextColor(Color.BLACK);
                    ((CheckBox)findViewById(R.id.CheckboxFY2)).setTextColor(Color.BLACK);
                    ((CheckBox)findViewById(R.id.CheckboxFY3)).setTextColor(Color.BLACK);
                }
                else{
                    ((CheckBox)findViewById(R.id.CheckboxY0)).setTextColor(Color.BLACK);
                    ((CheckBox)findViewById(R.id.CheckboxY1)).setTextColor(Color.BLACK);
                }
            }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
            if(item.getItemId() == R.id.night_mode) {
                if(item.getItemId() == R.id.night_mode) {
                    MainActivity.night_mode = !MainActivity.night_mode;
                    if(MainActivity.night_mode){
                        toolbar2.setBackgroundColor(getResources().getColor(R.color.colorNightMode));
                        main.setBackgroundColor(getResources().getColor(R.color.colorNightModeLayout));
                        if(cs.getChart()=="fifth") {
                            ((CheckBox) findViewById(R.id.CheckboxFY0)).setTextColor(Color.WHITE);
                            ((CheckBox) findViewById(R.id.CheckboxFY1)).setTextColor(Color.WHITE);
                            ((CheckBox) findViewById(R.id.CheckboxFY2)).setTextColor(Color.WHITE);
                            ((CheckBox) findViewById(R.id.CheckboxFY3)).setTextColor(Color.WHITE);
                        }
                        else{
                            ((CheckBox)findViewById(R.id.CheckboxY0)).setTextColor(Color.WHITE);
                            ((CheckBox)findViewById(R.id.CheckboxY1)).setTextColor(Color.WHITE);
                        }
                    }
                    else{
                        toolbar2.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        main.setBackgroundColor(Color.WHITE);
                        if(cs.getChart()=="fifth") {
                            ((CheckBox)findViewById(R.id.CheckboxFY0)).setTextColor(Color.BLACK);
                            ((CheckBox)findViewById(R.id.CheckboxFY1)).setTextColor(Color.BLACK);
                            ((CheckBox)findViewById(R.id.CheckboxFY2)).setTextColor(Color.BLACK);
                            ((CheckBox)findViewById(R.id.CheckboxFY3)).setTextColor(Color.BLACK);
                        }
                        else{
                            ((CheckBox)findViewById(R.id.CheckboxY0)).setTextColor(Color.BLACK);
                            ((CheckBox)findViewById(R.id.CheckboxY1)).setTextColor(Color.BLACK);
                        }
                    }
                    cs.postInvalidate();
                }
            }
            return true;
        }
        public static float clamp(float val, float min, float max) {
            return Math.max(min, Math.min(max, val));
        }



}
