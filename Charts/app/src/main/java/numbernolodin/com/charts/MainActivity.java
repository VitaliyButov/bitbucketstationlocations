package numbernolodin.com.charts;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import numbernolodin.com.charts.view.CustomView;

public class MainActivity extends AppCompatActivity {
    public Button b1,b2,b3,b4,b5;
    public static boolean night_mode;
    private Toolbar toolbar;
    LinearLayout firstLayout;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.night_mode, menu);
        return true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        firstLayout = (LinearLayout) findViewById(R.id.FirstLayout);
        night_mode = false;
        toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);

        //View currentLayout = LayoutInflater.from(getApplicationContext()).inflate(R.layout.activity_main,null);

        if(night_mode){
            toolbar.setBackgroundColor(getResources().getColor(R.color.colorNightMode));
            firstLayout.setBackgroundColor(getResources().getColor(R.color.colorNightModeLayout));
        }
        else{
            toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            firstLayout.setBackgroundColor(Color.WHITE);
        }
        b1 =(Button) findViewById(R.id.button);
        b2 =(Button) findViewById(R.id.button2);
        b3 =(Button) findViewById(R.id.button3);
        b4 =(Button) findViewById(R.id.button4);
        b5 =(Button) findViewById(R.id.button5);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomView.first = true;
                CustomView.second = false;
                CustomView.third = false;
                CustomView.fourth = false;
                CustomView.fifth = false;
                startActivity(new Intent(MainActivity.this, Main2Activity.class));

            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomView.first = false;
                CustomView.second = true;
                CustomView.third = false;
                CustomView.fourth = false;
                CustomView.fifth = false;
                startActivity(new Intent(MainActivity.this, Main2Activity.class));


            }
        });
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomView.first = false;
                CustomView.second = false;
                CustomView.third = true;
                CustomView.fourth = false;
                CustomView.fifth = false;
                startActivity(new Intent(MainActivity.this, Main2Activity.class));

            }
        });
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomView.first = false;
                CustomView.second = false;
                CustomView.third = false;
                CustomView.fourth = true;
                CustomView.fifth = false;
                startActivity(new Intent(MainActivity.this, Main2Activity.class));
            }
        });
        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomView.first = false;
                CustomView.second = false;
                CustomView.third = false;
                CustomView.fourth = false;
                CustomView.fifth = true;
                startActivity(new Intent(MainActivity.this, Main2Activity.class));
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if(night_mode){
            toolbar.setBackgroundColor(getResources().getColor(R.color.colorNightMode));
            firstLayout.setBackgroundColor(getResources().getColor(R.color.colorNightModeLayout));
        }
        else{
            toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            firstLayout.setBackgroundColor(Color.WHITE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.night_mode) {
            night_mode = !night_mode;
            if(night_mode){
                toolbar.setBackgroundColor(getResources().getColor(R.color.colorNightMode));
                firstLayout.setBackgroundColor(getResources().getColor(R.color.colorNightModeLayout));
            }
            else{
                toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                firstLayout.setBackgroundColor(Color.WHITE);
            }
        }
        return true;
    }

}
