﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ActorController : MonoBehaviour
{
    private Animator anim;
    private bool run, fly,rotating,rolling,jump2,jump3,rotating2,touch,endGame;
    private CharacterController controller;
    private float secondsPass, secondsPass2,stepXText1,stepYText1,stepXhandImage,stepYhandImage, stepXText2, stepYText2, stepXhandImage2, stepYhandImage2,stepScoreText,score;
    private new Camera camera;
    private Color FlipColor;

    public Sprite points1, points2, points3, points4, points5, points6, points7, points8, playBnt1, playBnt2, playBnt3, playBnt4, playBnt5, playBnt6, playBnt7, playBnt8;
    public Sprite red, green;
    public bool replay,willBeFly;
    public GameObject pivot1, pivot2,center,UIimage,playButton,progressBar,triangle1,triangle2,tint,tint2,textMessage,handImage,textMessage2, handImage2,score1,flipCount,confetti1,confetti2,
        endgameTint,star,levelText,completedText,crown,scoreText,points,playBtn;

    public float moveXFlyDown,moveYFlyDown,moveXFlyUp,moveYFlyUp,cameraRotate,cameraMoveX, cameraMoveZ, rotateManny,timeCoefficient, timeCoefficient2,rotatingSpeed,secondCoefficient,rotationX,
        rotationY, moveXFlyUp2,moveYFlyUp2,moveXFlyDown2,moveYFlyDown2, moveXFlyUp3, moveYFlyUp3, moveXFlyDown3, moveYFlyDown3;
    // Start is called before the first frame update
    void Start()
    {
        anim = gameObject.transform.GetComponent<Animator>();
        controller = gameObject.transform.GetComponent<CharacterController>();
        secondsPass = 0;
        rotating = false;
        rotating2 = false;
        rolling = false;
        touch = false;
        camera = Camera.main;
        willBeFly = true;
        timeCoefficient = 0.2f;
        timeCoefficient2 = 0.35f;
        moveXFlyDown = -0.035f;
        moveYFlyDown = -0.06f;
        moveXFlyUp = -0.03f;
        moveYFlyUp = 0.02f;
        moveXFlyUp2 = -0.05f;
        moveYFlyUp2 = 0.05f;
        moveXFlyDown2 = -0.037f;
        moveYFlyDown2 = -0.082f;
        moveXFlyUp3 = -0.05f;
        moveYFlyUp3 = 0.005f;
        moveXFlyDown3 = -0.08f;
        moveYFlyDown3 = -0.03f;
        cameraRotate = -0.29f;
        cameraMoveX = 0.07f;
        cameraMoveZ = 0.03f;
        rotateManny = 0.1f;
        rotatingSpeed = 300;
        secondCoefficient = 1;
        stepXText1 = 0.15f;
        stepYText1 = 0.15f;
        stepXhandImage = 0.05f;
        stepYhandImage = 0.05f;
        stepScoreText = 0.1f;
        FlipColor = new Color(1, 0.9013662f,0,1);
    }

    // Update is called once per frame
    void Update()
    {

        rotationX = gameObject.transform.eulerAngles.x;
        rotationY = gameObject.transform.eulerAngles.y;

        if (rotating && gameObject.transform.eulerAngles.x < 345 && gameObject.transform.eulerAngles.x > 310 && gameObject.transform.eulerAngles.y == 270 && !rotating2)
        {
            triangle1.GetComponent<Image>().sprite = green;
        }
        else if(rotating) triangle1.GetComponent<Image>().sprite = red;

        if (gameObject.transform.eulerAngles.x < 27 && gameObject.transform.eulerAngles.y == 270 && rotating2 || gameObject.transform.eulerAngles.x > 342 && gameObject.transform.eulerAngles.x < 360 && gameObject.transform.eulerAngles.y == 270 && rotating2)
        {
            triangle2.GetComponent<Image>().sprite = green;
        }
        else if (rotating2) triangle2.GetComponent<Image>().sprite = red;

        if (Input.GetTouch(0).phase == TouchPhase.Began && rotating && gameObject.transform.eulerAngles.x < 345 && gameObject.transform.eulerAngles.x > 310 && !rotating2 && !endGame) 
        {
            jump2 = true;
            triangle1.SetActive(false);
        }
        else if (Input.GetTouch(0).phase == TouchPhase.Ended && rotating2)
        {
            jump3 = true;
            triangle2.SetActive(false);
        }
        if (jump2 && gameObject.transform.eulerAngles.x<310&& gameObject.transform.eulerAngles.x > 300)
        {
            jump2 = false;
            anim.SetInteger("State", 3);
            StopAllCoroutines();
            tint.SetActive(false);
            rolling = true;
            StartCoroutine(Rolling());
            secondsPass = 0;
        }
        if(rolling && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            touch = true;
        }
        /*if(jump3 && gameObject.transform.eulerAngles.x < 330 && gameObject.transform.eulerAngles.x > 320 && gameObject.transform.eulerAngles.y == 270)
        {                        
            rotating2 = false;           
            StopAllCoroutines();
            tint.SetActive(false);
            tint2.SetActive(false);
            gameObject.AddComponent<Rigidbody>();
            gameObject.GetComponent<Rigidbody>().mass = 100;
            secondsPass = 0;                        
            StartCoroutine(MoveCamera());
            ActivateCollider();
            anim.SetInteger("State", 3);
            gameObject.GetComponent<Rigidbody>().AddForce(-Vector3.right * 1001, ForceMode.Impulse);
            gameObject.GetComponent<Rigidbody>().AddForce(Vector3.up * 601, ForceMode.Impulse);
            gameObject.GetComponent<Rigidbody>().AddTorque(-Vector3.forward*5000,ForceMode.Impulse);
            Invoke("Jumped", 1f);
            Invoke("ShowScore",1.5f);
            Invoke("EndGame",2f);
            jump3 = false;


        }*/

        if (replay)
        {
            StopAllCoroutines();
            run = false;
            fly = false;
            rolling = false;
            rotating = false;
            anim.SetBool("Replay", true);                    
            transform.localPosition = new Vector3(-25.08f, 12.76f,75.98f);
            transform.eulerAngles = new Vector3(0,-90,0);
            secondsPass = 0;
            camera.transform.position = new Vector3(-17.3f, 17.19f, 85.2f);
            camera.transform.eulerAngles = new Vector3(4.8f, 237.026f,1.333f);
            replay = false;
        }
    }

    private void LateUpdate()
    {
        if (jump3 && gameObject.transform.eulerAngles.x < 360 && gameObject.transform.eulerAngles.x > 320 && gameObject.transform.eulerAngles.y == 270)
        {
            rotating2 = false;
            StopAllCoroutines();
            tint.SetActive(false);
            tint2.SetActive(false);
            gameObject.AddComponent<Rigidbody>();
            gameObject.GetComponent<Rigidbody>().mass = 100;
            secondsPass = 0;
            StartCoroutine(MoveCamera());
            ActivateCollider();
            anim.SetInteger("State", 3);
            gameObject.GetComponent<Rigidbody>().AddForce(-Vector3.right * 1001, ForceMode.Impulse);
            gameObject.GetComponent<Rigidbody>().AddForce(Vector3.up * 601, ForceMode.Impulse);
            gameObject.GetComponent<Rigidbody>().AddTorque(-Vector3.forward * 5000, ForceMode.Impulse);
            Invoke("Jumped", 1f);
            Invoke("ShowScore", 1.5f);
            Invoke("EndGame", 2f);
            jump3 = false;


        }
    }
    void ActivateCollider()
    {        
        gameObject.GetComponent<CapsuleCollider>().enabled = true;
    }
    void Jumped()
    {
        endGame = true;
        anim.SetInteger("State", 2);
        flipCount.SetActive(true);
        flipCount.GetComponent<TextMeshProUGUI>().text = "x1 FLIP";
        flipCount.GetComponent<TextMeshProUGUI>().color = FlipColor;
        StartCoroutine(ShowMessage3(flipCount));
        confetti1.SetActive(true);
        confetti2.SetActive(true);
    }
    void ShowScore()
    {
        
        score = 20;
        score1.GetComponent<TextMeshProUGUI>().text = "20";
        flipCount.GetComponent<TextMeshProUGUI>().color = Color.white;
        flipCount.GetComponent<TextMeshProUGUI>().text = "+10 POINTS";
        StartCoroutine(ShowMessage3(flipCount));
        StartCoroutine(DeactivateFlipCount());
    }
    void EndGame()
    {        
        StartCoroutine(EndGameC());
    }
    public void Play()
    {
        anim.SetBool("Replay", false);
        anim.SetInteger("State", 1);
        run = true;
        StartCoroutine(Run());
        UIimage.SetActive(false);
        playButton.SetActive(false);
    }
    IEnumerator EndGameC()
    {
        for(int i = 0; i < 1; i++)
        {
            endgameTint.SetActive(true);
            yield return new WaitForSeconds(0.1f);
        }
        for (int i = 0; i < 1; i++)
        {
            star.SetActive(true);
            StartCoroutine(ShowMessage3(star));
            yield return new WaitForSeconds(0.1f);
        }
        for (int i = 0; i < 1; i++)
        {
            levelText.SetActive(true);
            StartCoroutine(ShowMessage3(levelText));
            yield return new WaitForSeconds(0.1f);
        }
        for (int i = 0; i < 1; i++)
        {
            completedText.SetActive(true);
            StartCoroutine(ShowMessage3(completedText));
            yield return new WaitForSeconds(0.1f);
        }
        for (int i = 0; i < 1; i++)
        {
            crown.SetActive(true);
            StartCoroutine(ShowMessage3(crown));
            yield return new WaitForSeconds(0.1f);
        }
        for (int i = 0; i < 1; i++)
        {
            scoreText.SetActive(true);
            StartCoroutine(ShowMessage3(scoreText));
            yield return new WaitForSeconds(0.1f);
        }
        for (int i = 0; i < 1; i++)
        {
            scoreText.SetActive(true);
            StartCoroutine(ShowMessage3(scoreText));
            yield return new WaitForSeconds(0.1f);
        }
        for (int i = 0; i < 1; i++)
        {
            points.SetActive(true);
            StartCoroutine(ShowMessage3(points));
            StartCoroutine(pointsView());
            yield return new WaitForSeconds(0.1f);
        }
        for (int i = 0; i < 1; i++)
        {
            playBtn.SetActive(true);
            StartCoroutine(ShowMessage3(playBtn));
            StartCoroutine(playBtnView());
            yield return new WaitForSeconds(0.1f);
        }
    }

    IEnumerator pointsView()
    {
        for(int i = 0; i < 1; i++)
        {
            points.GetComponent<Image>().sprite = points1;
            yield return null;
        }
        for (int i = 0; i < 1; i++)
        {
            points.GetComponent<Image>().sprite = points2;
            points.transform.localScale = new Vector3(points.transform.localScale.x+0.2f,points.transform.localScale.y,points.transform.localScale.z);
            yield return null;
        }
        for (int i = 0; i < 1; i++)
        {
            points.GetComponent<Image>().sprite = points3;
            points.transform.localScale = new Vector3(points.transform.localScale.x + 0.2f, points.transform.localScale.y, points.transform.localScale.z);
            yield return null;
        }
        for (int i = 0; i < 1; i++)
        {
            points.GetComponent<Image>().sprite = points4;
            points.transform.localScale = new Vector3(points.transform.localScale.x + 0.2f, points.transform.localScale.y, points.transform.localScale.z);
            yield return null;
        }
        for (int i = 0; i < 1; i++)
        {
            points.GetComponent<Image>().sprite = points5;
            points.transform.localScale = new Vector3(points.transform.localScale.x + 0.4f, points.transform.localScale.y, points.transform.localScale.z);
            yield return null;
        }
        for (int i = 0; i < 1; i++)
        {
            points.GetComponent<Image>().sprite = points6;
            points.transform.localScale = new Vector3(points.transform.localScale.x + 0.4f, points.transform.localScale.y, points.transform.localScale.z);
            yield return null;
        }
        for (int i = 0; i < 1; i++)
        {
            points.GetComponent<Image>().sprite = points7;
            points.transform.localScale = new Vector3(points.transform.localScale.x + 0.4f, points.transform.localScale.y, points.transform.localScale.z);
            yield return null;
        }
        for (int i = 0; i < 1; i++)
        {
            points.GetComponent<Image>().sprite = points8;
            points.transform.localScale = new Vector3(points.transform.localScale.x + 0.4f, points.transform.localScale.y, points.transform.localScale.z);
            yield return null;
        }
    }
    IEnumerator playBtnView()
    {
        for (int i = 0; i < 1; i++)
        {
            playBtn.GetComponent<Image>().sprite = playBnt1;
            yield return null;
        }
        for (int i = 0; i < 1; i++)
        {
            playBtn.GetComponent<Image>().sprite = playBnt2;
            playBtn.transform.localScale = new Vector3(playBtn.transform.localScale.x + 0.2f, playBtn.transform.localScale.y, playBtn.transform.localScale.z);
            yield return null;
        }
        for (int i = 0; i < 1; i++)
        {
            playBtn.GetComponent<Image>().sprite = playBnt3;
            playBtn.transform.localScale = new Vector3(playBtn.transform.localScale.x + 0.2f, playBtn.transform.localScale.y, playBtn.transform.localScale.z);
            yield return null;
        }
        for (int i = 0; i < 1; i++)
        {
            playBtn.GetComponent<Image>().sprite = playBnt4;
            playBtn.transform.localScale = new Vector3(playBtn.transform.localScale.x + 0.2f, playBtn.transform.localScale.y, playBtn.transform.localScale.z);
            yield return null;
        }
        for (int i = 0; i < 1; i++)
        {
            playBtn.GetComponent<Image>().sprite = playBnt5;
            playBtn.transform.localScale = new Vector3(playBtn.transform.localScale.x + 0.4f, playBtn.transform.localScale.y, playBtn.transform.localScale.z);
            yield return null;
        }
        for (int i = 0; i < 1; i++)
        {
            playBtn.GetComponent<Image>().sprite = playBnt6;
            playBtn.transform.localScale = new Vector3(playBtn.transform.localScale.x + 0.4f, playBtn.transform.localScale.y, playBtn.transform.localScale.z);
            yield return null;
        }
        for (int i = 0; i < 1; i++)
        {
            playBtn.GetComponent<Image>().sprite = playBnt7;
            playBtn.transform.localScale = new Vector3(playBtn.transform.localScale.x + 0.4f, playBtn.transform.localScale.y, playBtn.transform.localScale.z);
            yield return null;
        }
        for (int i = 0; i < 1; i++)
        {
            playBtn.GetComponent<Image>().sprite = playBnt8;
            playBtn.transform.localScale = new Vector3(playBtn.transform.localScale.x + 0.4f, playBtn.transform.localScale.y, playBtn.transform.localScale.z);
            yield return null;
        }
    }
    IEnumerator MoveCamera()
    {

        for(int i=0; i < 40; i++)
        {
            camera.transform.position = new Vector3(camera.transform.position.x - 0.3f, camera.transform.position.y, camera.transform.position.z);
            yield return new WaitForSeconds(0.01f);
        }
        
    }
    IEnumerator Run()
    {
        while (run && secondsPass < 0.2f)
        {
            controller.Move(transform.forward * 0.5f);
            camera.transform.position = new Vector3(camera.transform.position.x - 0.5f, camera.transform.position.y, camera.transform.position.z);
            secondsPass += 0.01f;
            yield return new WaitForSeconds(0.01f);
        }
        anim.SetInteger("State", 2);
        secondsPass = 0;
        secondsPass2 = 0;
        fly = true;
        StartCoroutine(Fly());
        progressBar.SetActive(true);
        score1.SetActive(true);
    }

    IEnumerator Fly()
    {        
        while (fly && secondsPass <= 1.8f*timeCoefficient)
        {
            controller.Move(new Vector3(moveXFlyUp / timeCoefficient, moveYFlyUp / timeCoefficient, 0f));
            gameObject.transform.localEulerAngles = new Vector3(transform.localEulerAngles.x + rotateManny / timeCoefficient, transform.localEulerAngles.y,transform.localEulerAngles.z);           
            camera.transform.Rotate(0,cameraRotate / timeCoefficient, 0);
            camera.transform.position = new Vector3(camera.transform.position.x - cameraMoveX / timeCoefficient, camera.transform.position.y, camera.transform.position.z+ cameraMoveZ / timeCoefficient);
            //Debug.Log(secondsPass);
            secondsPass += 0.01f;
            yield return new WaitForSeconds(0.01f);
        }
         while (fly && secondsPass > 1.8f * timeCoefficient && secondsPass2 < 1.2f * timeCoefficient2 && willBeFly)
        {
           // Debug.Log(secondsPass);
            controller.Move(new Vector3(moveXFlyDown / timeCoefficient2, moveYFlyDown / timeCoefficient2, 0f));
            gameObject.transform.localEulerAngles = new Vector3(transform.localEulerAngles.x + rotateManny / timeCoefficient2, transform.localEulerAngles.y , transform.localEulerAngles.z);           
            camera.transform.position = new Vector3(camera.transform.position.x - cameraMoveX / timeCoefficient2, camera.transform.position.y, camera.transform.position.z + cameraMoveZ / timeCoefficient2);
            secondsPass2 += 0.01f;
            yield return new WaitForSeconds(0.01f);
        }
            secondsPass = 0;
        fly = false;
        rotating = true;
        StartCoroutine(Rotating(pivot1.transform.position));
    }

    IEnumerator Rotating(Vector3 pivot)
    {
        if (rotating)
        {
            triangle1.SetActive(true);
            triangle2.SetActive(false);
            tint.SetActive(true);
            textMessage.transform.localScale = new Vector3(stepXText1,stepYText1,textMessage.transform.localScale.z);
            handImage.transform.localScale = new Vector3(stepXhandImage,stepYhandImage,transform.localScale.z);
            StartCoroutine(ShowMessage1());
        }
        if (rotating2)
        {
            triangle1.SetActive(false);
            triangle2.SetActive(true);
            score = 10;
            score1.GetComponent<TextMeshProUGUI>().text = "10";
            flipCount.GetComponent<TextMeshProUGUI>().color = Color.white;
            flipCount.GetComponent<TextMeshProUGUI>().text = "+10 POINTS";
            StartCoroutine(ShowMessage3(flipCount));
            StartCoroutine(DeactivateFlipCount());
        }
        while (rotating)
        {
            gameObject.transform.RotateAround(pivot,-Vector3.forward,rotatingSpeed*Time.deltaTime);
            yield return new WaitForSeconds(0.01f);
        }
    }

    IEnumerator DeactivateFlipCount()
    {
        for(int i = 0; i < 1; i++) {
            yield return new WaitForSeconds(0.1f);
        }
        
        for(int i = 0; i < 10; i++)
        {
            flipCount.GetComponent<TextMeshProUGUI>().color = new Color(flipCount.GetComponent<TextMeshProUGUI>().color.r, flipCount.GetComponent<TextMeshProUGUI>().color.g, flipCount.GetComponent<TextMeshProUGUI>().color.b, flipCount.GetComponent<TextMeshProUGUI>().color.a-0.1f);
            yield return new WaitForSeconds(0.1f);
        }
        flipCount.SetActive(false);
    }

    IEnumerator Rolling()
    {
        while (rolling && secondsPass<0.2f)
        {
            gameObject.transform.RotateAround(center.transform.position,-Vector3.forward, 10);
            controller.Move(new Vector3(moveXFlyUp2/ timeCoefficient2, moveYFlyUp2 / timeCoefficient2, 0f));
            camera.transform.position = new Vector3(camera.transform.position.x-0.2f, camera.transform.position.y-0.06f, camera.transform.position.z);
            secondsPass += 0.01f;
            yield return new WaitForSeconds(0.01f);
        }
        flipCount.SetActive(true);
        flipCount.GetComponent<TextMeshProUGUI>().color = new Color(flipCount.GetComponent<TextMeshProUGUI>().color.r, flipCount.GetComponent<TextMeshProUGUI>().color.g, flipCount.GetComponent<TextMeshProUGUI>().color.b, 1);
        StartCoroutine(ShowMessage3(flipCount));
        anim.SetInteger("State",2);
        Time.timeScale = 0;
        tint2.SetActive(true);        
        textMessage2.transform.localScale = new Vector3(stepXText2, stepYText2, textMessage2.transform.localScale.z);
        handImage2.transform.localScale = new Vector3(stepXhandImage2, stepYhandImage2, transform.localScale.z);
        StartCoroutine(ShowMessage2());
        while (!touch)
        {
            yield return null;
        }
        Time.timeScale = 1;
        tint2.SetActive(false);
        while (rolling && secondsPass > 0.2 && secondsPass<0.4)
        {
            gameObject.transform.RotateAround(center.transform.position, -Vector3.forward, 4);
            controller.Move(new Vector3(moveXFlyDown2 / timeCoefficient2, moveYFlyDown2 / timeCoefficient2, 0f));
            camera.transform.position = new Vector3(camera.transform.position.x - 0.2f, camera.transform.position.y - 0.06f, camera.transform.position.z);
            secondsPass += 0.01f;
            yield return new WaitForSeconds(0.01f);
        }
        rotating2 = true;
        StartCoroutine(Rotating(pivot2.transform.position));        
        
    }
    IEnumerator Rolling2()
    {
        
        while (rolling && secondsPass<0.2f)
        {
            gameObject.transform.RotateAround(center.transform.position,-Vector3.forward, 10);
            secondsPass += 0.01f;
            yield return new WaitForSeconds(0.01f);
        }
        flipCount.SetActive(true);
        flipCount.GetComponent<TextMeshProUGUI>().color = FlipColor;
        StartCoroutine(ShowMessage3(flipCount));
        anim.SetInteger("State", 2);

    }
    IEnumerator ShowMessage1()
    {
        for (int i=0; i < 30; i++)
        {
            textMessage.transform.localScale = new Vector3(textMessage.transform.localScale.x+stepXText1, textMessage.transform.localScale.y+stepYText1, textMessage.transform.localScale.z);
            handImage.transform.localScale = new Vector3(handImage.transform.localScale.x+stepXhandImage, handImage.transform.localScale.y+stepYhandImage, transform.localScale.z);
            yield return null;
        }
        for(int i=0;i < 10; i++)
        {
            textMessage.transform.localScale = new Vector3(textMessage.transform.localScale.x - stepXText1, textMessage.transform.localScale.y - stepYText1, textMessage.transform.localScale.z);
            handImage.transform.localScale = new Vector3(handImage.transform.localScale.x - stepXhandImage, handImage.transform.localScale.y - stepYhandImage, transform.localScale.z);
            yield return null;
        }
    }
    IEnumerator ShowMessage2()
    {
        for (int i = 0; i < 30; i++)
        {
            textMessage2.transform.localScale = new Vector3(textMessage2.transform.localScale.x + stepXText1, textMessage2.transform.localScale.y + stepYText1, textMessage2.transform.localScale.z);
            handImage2.transform.localScale = new Vector3(handImage2.transform.localScale.x + stepXhandImage, handImage2.transform.localScale.y + stepYhandImage, transform.localScale.z);
            yield return null;
        }
        for (int i = 0; i < 10; i++)
        {
            textMessage2.transform.localScale = new Vector3(textMessage2.transform.localScale.x - stepXText1, textMessage2.transform.localScale.y - stepYText1, textMessage2.transform.localScale.z);
            handImage2.transform.localScale = new Vector3(handImage2.transform.localScale.x - stepXhandImage, handImage2.transform.localScale.y - stepYhandImage, transform.localScale.z);
            yield return null;
        }
    }

    IEnumerator ShowMessage3(GameObject text)
    {
        for(int i = 0;i<10; i++)
        {
            text.transform.localScale = new Vector3(text.transform.localScale.x + stepScoreText, text.transform.localScale.y + stepScoreText, text.transform.localScale.z);
            yield return null;
        }
        for(int i = 0; i<10; i++)
        {
            text.transform.localScale = new Vector3(text.transform.localScale.x - stepScoreText, text.transform.localScale.y - stepScoreText, text.transform.localScale.z);
        }
    }
}